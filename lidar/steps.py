from task_manager import BaseStep


class SetLidarToleranceStep(BaseStep):

    name = 'SetLidarToleranceStep'

    def __init__(self, tolerance, step):
        self.tolerance = tolerance
        self.step = step

        super().__init__()

    def run(self):
        from . import lidar

        print('Setting lidar tolerance')

        with lidar.change_tolerance(tolerance=self.tolerance):
            self.step.run()
