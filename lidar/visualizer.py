from queue import Empty as QueueEmpty
import numpy as np
import tkinter as tk


CIRCLE_SIZE = 5


def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return (rho, phi)


def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return (x, y)


class Visualizer():
    def __init__(self, queue):
        self.queue = queue

        self._root = tk.Tk()

        self._frame = tk.Frame(self._root)
        self._size = 800
        self._size = 800
        self._canvas = tk.Canvas(
            width=self._size,
            height=self._size,
            bg='black',
            borderwidth=0,
        )
        self._canvas.pack(side="top", fill="both", expand=True)

        self._root.after(0, self.redraw)

        self._root.mainloop()

    def redraw(self):
        try:
            points = self.queue.get(0)

            self._canvas.delete('all')

            self._canvas.create_line(
                (self._size / 2) - 10,
                (self._size / 2),
                (self._size / 2) + 10,
                (self._size / 2),
                fill='white',
            )

            self._canvas.create_line(
                (self._size / 2),
                (self._size / 2) - 10,
                (self._size / 2),
                (self._size / 2) + 10,
                fill='white',
            )

            for _, phi, rho in points:
                x, y = (
                    np.interp(x, [-1000, 1000], [0, self._size])
                    for x in pol2cart(rho=rho, phi=(phi - 90) / 180 * np.pi)
                )

                self._canvas.create_oval(x - CIRCLE_SIZE, y - CIRCLE_SIZE, x + CIRCLE_SIZE, y + CIRCLE_SIZE, fill='red')

        except QueueEmpty:
            pass

        finally:
            self._root.after(1, self.redraw)
