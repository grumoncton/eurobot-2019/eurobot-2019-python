import time
import threading
from queue import Queue
from rplidar import RPLidar

from lib import ContextManager, dotenv


LIDAR_OFFSET = float(dotenv.get_value('LIDAR_OFFSET'))


class LidarManager:
    # pylint: disable=too-many-instance-attributes

    def __init__(self, port):
        self.port = port
        self.lidar = None
        self.stop_event = threading.Event()
        self.ready = threading.Event()

        self.is_blocked = False

        self._DEFAULT_TOLERANCE = 400
        self.tolerance = self._DEFAULT_TOLERANCE
        self._FALSE_POSITIVE_THRESHOLD = 2

        self._MID_ANGLE = None
        self.set_angle(0)

        # Include values to the right and left of mid angle
        self._ANGLE_SPREAD = 25

    def init(self):
        self.lidar = RPLidar(port=self.port)

        return ContextManager(on_exit=self.stop)

    def set_angle(self, angle):
        angle = angle % 360
        self._MID_ANGLE = angle + LIDAR_OFFSET

    def _should_include_angle(self, angle):
        """
        Check if the given angle is within the spread of angles to include.
        """

        delta = abs((self._MID_ANGLE - angle + 360 + 180) % 360 - 180)

        return delta <= self._ANGLE_SPREAD

    def _is_measure_blocking(self, measure):
        """
        Check if we are blocked by the obstacle giving a measure.
        """

        _, angle, distance = measure

        return self._FALSE_POSITIVE_THRESHOLD < distance < self.tolerance and \
            self._should_include_angle(angle)

    def mainloop(self):
        time.sleep(5)

        try:
            for scan in self.lidar.iter_scans():
                self.ready.set()

                blocking_measures = [
                    measure for measure in scan
                    if self._is_measure_blocking(measure)
                ]

                self.is_blocked = bool(blocking_measures)

                if self.is_blocked:
                    self.stop_event.set()

        finally:
            self.lidar.stop()
            self.lidar.stop_motor()
            self.lidar.disconnect()

    def visualizer(self):
        from lidar.visualizer import Visualizer

        def loop(queue):
            time.sleep(5)

            for scan in self.lidar.iter_scans():
                queue.put(scan)

        queue = Queue()

        thread = threading.Thread(target=loop, args=(queue,))
        thread.start()

        Visualizer(queue=queue)

    def run_in_thread(self):
        thread = threading.Thread(target=self.mainloop, daemon=True)
        thread.start()

        return thread

    def stop(self):
        self.lidar.stop()
        self.lidar.stop_motor()
        self.lidar.disconnect()

    def _reset_tolerance(self):
        self.tolerance = self._DEFAULT_TOLERANCE

    def change_tolerance(self, tolerance):
        self.tolerance = tolerance

        return ContextManager(on_exit=self._reset_tolerance)


lidar = LidarManager(port='/dev/ttyUSB0')
