import time
import logging

from utils.retry import Retry
from .crc import crc16, InvalidChecksumException


logger = logging.getLogger(__name__)

MAX_RETRIES = 10


def get_call_response_interval():
    if logger.getEffectiveLevel() == logging.DEBUG:
        return 1
    return 0.1


class ResendError(IOError):
    """
    Error raised when slave asks us to resend data.
    """


def _parse_response(response):
    """
    Check checksum of given string and return message portion.
    """

    from . import CRC_LENGTH

    # Split up response
    message = response[:-CRC_LENGTH]
    crc = response[-CRC_LENGTH:]

    # Convert bytes of checksum to int
    crc = int.from_bytes(
        map(ord, crc) if isinstance(crc, str) else crc,
        byteorder='little',
    )

    # Check checksum
    if crc != crc16(message):
        raise InvalidChecksumException(
            'Checksum error (recived: 0x{0:x}, calculated: 0x{1:x})'
            .format(crc, crc16(message)),
        )

    return message


def _send_message(device, instruction, args=None):
    """
    Calculate checksum of given string and send full message on serial port.
    """

    from . import ARG_LENGTH, CRC_LENGTH, i2c

    if not args:
        args = tuple()

    assert len(args) < ARG_LENGTH, \
        'Command too long (length: {length}, max: {max_length}).' \
        .format(length=len(args), max_length=ARG_LENGTH)

    if isinstance(args, str):
        args = tuple(map(ord, args))

    # Right-pad args with ones
    args = list(args) + [0xff] * (ARG_LENGTH - len(args))

    # Calculate checksum for message
    crc = crc16([instruction] + args)

    frame = bytes(args) + crc.to_bytes(CRC_LENGTH, byteorder='little')

    logger.debug(
        'Sending instruction 0x%(instruction)x to 0x%(addr)x with args '
        '%(args)s and crc %(crc)d.',
        {
            'addr': device.addr,
            'instruction': instruction,
            'args': ':'.join('{0:02x}'.format(b) for b in args),
            'crc': crc,
        },
    )

    i2c.write_block(
        addr=device.addr,
        instruction=instruction,
        args=frame,
    )


def _send_message_and_get_response(device, instruction, args=None):
    """
    Send same message after every timeout until valid response received.
    """

    from . import BaseInstruction, i2c

    _send_message(device=device, instruction=instruction, args=args)

    logger.debug('Sent instruction. Awaiting response.')

    time.sleep(get_call_response_interval())

    logger.debug('Requesting response from 0x%(addr)x', {'addr': device.addr})

    response = i2c.read_block(
        addr=device.addr,
        instruction=BaseInstruction.GET_RESPONSE,
    )

    try:
        return _parse_response(response)

    except InvalidChecksumException as e:
        _send_message_and_get_response(
            device=device,
            instruction=BaseInstruction.NACK,
        )
        raise e


@Retry(MAX_RETRIES)
def send_instruction(device, instruction, args=None):
    """
    Write instruction to device.

    Send instruction, then perform read for validation (ack and checksum).
    """

    from time_up_handler import time_up_event
    from . import BaseInstruction

    if time_up_event.is_set():
        return

    response, *_ = _send_message_and_get_response(
        device=device,
        instruction=instruction,
        args=args,
    )

    # If device asked to resend
    if response == BaseInstruction.NACK:
        raise ResendError()

    if response != BaseInstruction.ACK:
        logger.warning('Received %(response)s from %(addr)x. Expected ACK.', {
            'response': response,
            'addr': device.addr,
        })
        raise ResendError()
    time.sleep(get_call_response_interval())


@Retry(MAX_RETRIES)
def read_data(device, instruction, args=None):
    """
    Read data from device.

    Send command to select data, then perform read.
    """

    from . import BaseInstruction

    response = _send_message_and_get_response(
        device=device,
        instruction=instruction,
        args=args,
    )

    # If device asked to resend
    if response[0] == BaseInstruction.NACK:
        raise ResendError()

    time.sleep(get_call_response_interval())
    return response
