POLYNOMIAL = 0x1021
PRESET = 0


class InvalidChecksumException(IOError):
    def __init__(self, message=None):
        super().__init__(message or 'Checksum calculated and '
                         'checksum received differ')


def _initial(c):
    crc = 0
    c = c << 8
    for _ in range(8):
        if (crc ^ c) & 0x8000:
            crc = (crc << 1) ^ POLYNOMIAL
        else:
            crc = crc << 1
        c = c << 1
    return crc


_tab = tuple(_initial(i) for i in range(256))


def _update_crc(crc, c):
    cc = 0xff & c

    tmp = (crc >> 8) ^ cc
    crc = (crc << 8) ^ _tab[tmp & 0xff]
    crc = crc & 0xffff

    return crc


def crc16(message):
    """
    Calculate 2-byte checksum of given string.
    """

    crc = PRESET
    for c in message:
        crc = _update_crc(crc, ord(c) if isinstance(c, str) else c)
    return crc
