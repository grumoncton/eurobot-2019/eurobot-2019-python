class OpenSerialPort:
    """
    Open serial port on enter. Close on exit.
    ```python
    with OpenSerialPort(serial.Serial()):
        pass
    ```
    ```python
    with OpenSerialPort(serial.Serial(), '/dev/ttyUSB0'):
        pass
    ```
    """

    def __init__(self, serial_interface, port=None):
        self.serial_interface = serial_interface
        self.port = port

    def __enter__(self):
        if self.port is not None:
            self.serial_interface.port = self.port

        if not self.serial_interface.is_open:
            self.serial_interface.open()

    def __exit__(self, *args, **kwargs):
        self.serial_interface.close()
