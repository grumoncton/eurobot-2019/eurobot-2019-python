# comms

# comms.serial_utils

## OpenSerialPort
```python
OpenSerialPort(self, serial_interface, port=None)
```

Open serial port on enter. Close on exit.
```python
with OpenSerialPort(serial.Serial()):
    pass
```
```python
with OpenSerialPort(serial.Serial(), '/dev/ttyUSB0'):
    pass
```

# comms.crc

## crc16
```python
crc16(str)
```
Calculate 2-byte checksum of given string
# comms.comms

## send_command
```python
send_command(device, command)
```

Send given string on serial port handling retries, timeout, and checksums
