<!--
vim: spelllang=fr keymap=accents
-->

# Robot Comms

Ce dossier contient les fichiers responsables pour la communication entre le
contrôleur (_Raspberry Pi_) et les périphériques (microcontrôleurs C).

## Format des messages

Les messages ont une longueur maximale de 32 octets. Puisque le dernier octet du
message doit être la fin de ligne (`'\n'`) et les deux octets avant doivent être
la somme de contrôle, la longueur maximale de la commande est de 29 octets.
L'exemple ci-dessous montre le message complet avec somme de contrôle et fin de
ligne pour la commande `'Hello world!'`. Ici, `0xdb` et `0x39` sont
respectivement l'octet le moins significatif et l'octet le plus significatif de
la somme de contrôle.

```python
['H', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd', '!', 0xdb, 0x39, '\n']
```

Les sommes de contrôles sont calculées avec l'algorithme CRC-CCITT XMODEM. Si la
somme de contrôle calculée ne correspond pas à celle dans le message, le
programme enverra un message qui spécifie que l'autre partie devrait renvoyer
son message.  L'utilisation des sommes de contrôles permet d'assurer que les
messages sont bien reçu.  Cela est très important dans un robot avec beaucoup de
signaux qui propagent dans une petite volume. Il y a un grand chance
d'interférence dans cette situation.

Le programme utilise aussi des erreurs d'expiration de délai (timeout) afin
d'assurer que les messages sont reçus. En effet, si une réponse n'est pas reçue
avant une certaine délaie, la requête en envoyée une deuxième fois.

Similaire ment au moyen que le contrôleur utilise les sommes de contrôles et
les erreurs d'expiration de délai afin d'assurer que les périphériques reçoit
correctement les messages, les périphériques utilisent les mêmes techniques
afin d'assurer que le contrôleur reçoit les messages. Ainsi, à chaque fois
qu'une conversation est terminée, il faut envoyer un code de confirmation.
Sinon, l'autre partie enverra à nouveau son message à chaque fois que le délai
d'expiration est atteint.

Un exemple de communication standard entre le contrôleur et un périphérique est
donnée ci-dessous.

```
+--------------+                                               +--------------+
|              | ------------------ Request -----------------> |              |
|  Controller  | <----------------- Response ----------------- |  Peripheral  |
|              | -------------------  Ack  ------------------> |              |
+--------------+                                               +--------------+
```

L'exemple ci-dessous montre le fonctionnement lorsque les deux erreurs
corrigibles se produisent. Premièrement, la réponse du périphérique n'est pas reçu
par le contrôleur. Cela peut se produire si la requête est perdue, si la réponse
est perdue ou si le périphérique n'était pas prêt à traiter la requête. Par la
suite, lorsque le contrôleur envoi sa requête pour la deuxième fois, la somme de
contrôle calculée ne correspond pas à celle dans le message. Ainsi, le
périphérique envoi un message indiquant que la requête doit être envoyée encore.

```
+--------------+                                               +--------------+
|              | ------------------ Request -----------------> |              |
|              |                   (Timeout)                   |              |
|              | ------------------ Request -----------------> |              |
|  Controller  |             (Incorrect checksum)              |  Peripheral  |
|              | <------------ "RESEND" request -------------- |              |
|              | ------------------ Request -----------------> |              |
|              | <----------------- Response ----------------- |              |
|              | -------------------  Ack  ------------------> |              |
+--------------+                                               +--------------+
```

## Documentation des fonctions
Une description des fonctions de communication est donnée ci-dessous.

### comms

### comms.serial_utils

#### OpenSerialPort
```python
OpenSerialPort(self, serial_interface, port=None)
```

Open serial port on enter. Close on exit.
```python
with OpenSerialPort(serial.Serial()):
    pass
```
```python
with OpenSerialPort(serial.Serial(), '/dev/ttyUSB0'):
    pass
```

### comms.crc

#### crc16
```python
crc16(str)
```
Calculate 2-byte checksum of given string
### comms.comms

#### send_command
```python
send_command(device, command)
```

Send given string on serial port handling retries, timeout, and checksums

