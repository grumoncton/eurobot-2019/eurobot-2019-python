from enum import IntEnum, unique


@unique
class BaseInstruction(IntEnum):
    ACK = 0x06
    NACK = 0x15
    GET_RESPONSE = 0x10
    GET_INFO = 0x11
    ENTER_BOOTLOADER = 0xa1
    SET_TABLE_SIDE = 0x12
    GET_STATUS = 0x13
