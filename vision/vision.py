import cv2
import numpy as np

from . import adjustments

try:
    import picamera
except ImportError:
    pass


def take_picture():
    import datetime
    from os import path

    filename = path.join('/tmp', 'pucks-{}.jpg'.format(
        datetime.datetime.now().isoformat(),
    ))

    with picamera.PiCamera() as camera:
        camera.resolution = (960, 540)
        camera.brightness = 45
        camera.contrast = 0
        camera.capture(filename)

    return filename


# Area threshold
# AREA_THRESH = 7500
AREA_THRESH = 2000


# PROCESSING

def filter_colour(img_arr, colour_str):
    """Filtre la couleur `colour_str` dans l'image `img_arr`

    Args:
        img_arr (np.ndarray): Array contenant une image en BGR
        colour_str (str): Nom de la couleur

    """

    # GREEN EXAMPLES
    # (36, 25, 25), (70, 255,255)
    # (36,0,0), (86,255,255)

    # Couleurs

    # Colours for Salle Michelin as of 18/05/2019
    COLOURS = dict(green=dict(minimum=np.array([25, 25, 25], np.uint8),
                              maximum=np.array([51, 255, 255], np.uint8)),
                   blue=dict(minimum=np.array([85, 100, 100], np.uint8),
                             maximum=np.array([115, 255, 255], np.uint8)))

    # Récupération des couleurs dans le dictionnaire COLOURS
    lwr_hsv = COLOURS[colour_str]["minimum"]
    upr_hsv = COLOURS[colour_str]["maximum"]

    # Convertit l'image en HSV
    hsv = cv2.cvtColor(img_arr, cv2.COLOR_BGR2HSV)

    # Créé un masque entre lew_hsv et upr_hsv
    mask = cv2.inRange(hsv, lwr_hsv, upr_hsv)

    # Soigner le masque
    kernel = np.ones((5, 5), np.uint8)
    # mask = cv2.dilate(mask, kernel, iterations=1)
    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
    _, mask = cv2.threshold(mask, 173, 255, cv2.THRESH_BINARY)

    return mask


def find_puck_centers(bw_mask):
    """Trouve les centres des rondelles dans le masque `bw_mask` et l'image
     de résultat `rgb_res` pour la couleur `colour`

    Args:
        rgb_res (np.ndarray): Array contenant l'image filtrée
        bw_mask (np.ndarray): Array du masque, en noir et blanc
        colour (str): Nom de couleur

    """

    edges = cv2.Canny(bw_mask, 50, 150)
    contours = cv2.findContours(
        edges,
        cv2.RETR_EXTERNAL,
        cv2.CHAIN_APPROX_SIMPLE,
    )[-2]

    # TEST
    # Voir les contours
    cv2.drawContours(bw_mask, contours, -1, (0, 0, 0), 3)

    edges = cv2.Canny(bw_mask, 50, 150)
    contours = cv2.findContours(
        edges,
        cv2.RETR_EXTERNAL,
        cv2.CHAIN_APPROX_SIMPLE,
    )[-2]

    if cv2.countNonZero(bw_mask) <= 0:
        return None

    contours_with_areas = [{
        'contour': contour,
        'area': cv2.contourArea(contour),
    } for contour in contours]

    biggest = max(
        contours_with_areas,
        key=lambda d: d['area'],
    )

    if biggest['area'] < AREA_THRESH:
        return None

    x, y, w, h = cv2.boundingRect(biggest['contour'])
    cx = int(x + w / 2)
    cy = int(y + h / 2)
    return cx, cy


def process_image(img_name, colour):
    """Traite une image

    Args:
        img_name (str): Nom du fichier d'image (Ex : `pucks`)

    Returns:
        dict: File centers with keys `blue` and `green`

    """

    # Importe la photo
    img = cv2.imread(img_name)

    # Region of image
    img = img[:int(450 / adjustments.N_ADJ), :]

    # Traite l'image
    mask = filter_colour(img, colour)
    col_centers = find_puck_centers(mask)

    return col_centers


def find_blue():
    photo_filename = take_picture()
    center = process_image(photo_filename, "blue")
    adjusted = adjustments.adjust_center(center)

    return adjusted


def find_green():
    photo_filename = take_picture()
    center = process_image(photo_filename, "green")
    adjusted = adjustments.adjust_center(center)

    return adjusted


if __name__ == "__main__":
    print('Find blue', find_blue())
    print('Find blue', find_green())
