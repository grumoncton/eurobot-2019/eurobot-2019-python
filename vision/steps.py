import time
import logging

from lib import Coord, TableSide, dotenv
from task_manager import BaseStep, run_steps_in_parallel, TimeoutStep
from devices import wheels
from devices.secondary_mechanisms.scara_steps import ScaraMoveToStep, \
    ScaraStashStep, ScaraSpoonStep
from devices.secondary_mechanisms.steps import EnableVacuumStep, \
    DisableVacuumStep
from devices.secondary_mechanisms.leadscrew import LeadscrewStep, LeadscrewState


logger = logging.getLogger(__name__)

TABLE_SIDE = TableSide(str(dotenv.get_value('TABLE_SIDE')))


class NoPuckError(RuntimeError):
    """
    No pucks were found by picam
    """


class PickUpCaosPuckWithVision(BaseStep):
    name = 'PickUpCaosPuckWithVision'

    def __init__(self, puck_finder):
        self.puck_finder = puck_finder

        self._num_no_puck_error = 0
        self._MAX_NO_PUCK_ERROR = 2

        self._pucks_we_think_we_got = 0
        self._MAX_ATTEMPTED_PICK_UPS = 2

        self.BACKUP_DISTANCE = 50

        super().__init__()

    def find_puck(self):
        logger.info('Looking for puck')

        puck = self.puck_finder()

        if not puck:
            logger.warning('No pucks found.')
            raise NoPuckError()

        logger.debug('Found puck at %(coord)s.', {'coord': puck})

        puck = Coord(*puck)
        if puck.y < 50:
            logger.warning('Too close. Must have picked up robot.')
            raise NoPuckError()

        if TABLE_SIDE == TableSide.PURPLE:
            puck = puck.from_prime()

        return puck

    def pickup_puck(self):
        puck = self.find_puck()
        should_back_up = puck.hypot() < 100 or puck.y < 90

        run_steps_in_parallel(steps=[
            ScaraMoveToStep(puck)
        ] if not should_back_up else [
            ScaraMoveToStep(Coord(x=puck.x, y=puck.y + self.BACKUP_DISTANCE)),
            wheels.DriveStraightStep(
                distance=self.BACKUP_DISTANCE,
                direction=wheels.Wheels.Direction.BACKWARDS,
            ),
        ])

        # Pick up puck
        EnableVacuumStep().run()
        LeadscrewStep(state=LeadscrewState.PUCK_PICK_UP_HEIGHT).run()

        run_steps_in_parallel(steps=[
            LeadscrewStep(state=LeadscrewState.CLEAR_STASH_HEIGHT),
        ] + ([TimeoutStep(
            seconds=0.5,
            step=wheels.DriveStraightStep(distance=self.BACKUP_DISTANCE),
        )] if should_back_up else []))

    def run(self):
        while True:
            try:
                ScaraMoveToStep(Coord(x=-140, y=40)).run()

                self.pickup_puck()

                self._pucks_we_think_we_got += 1
                if self._pucks_we_think_we_got >= self._MAX_ATTEMPTED_PICK_UPS:
                    break

            except NoPuckError:
                if self._num_no_puck_error <= self._MAX_NO_PUCK_ERROR:
                    self._num_no_puck_error += 1
                    continue

                break


class StashCaosPuckWithVision(PickUpCaosPuckWithVision):
    name = 'StashCaosPuckWithVision'

    def __init__(self, side, puck_finder):
        self.side = side

        super().__init__(puck_finder=puck_finder)

    def run(self):
        while True:
            try:
                ScaraSpoonStep(side=self.side).run()

                self.pickup_puck()

                # Stash puck
                logger.info('Stashing puck')
                ScaraStashStep(side=self.side).run()
                time.sleep(0.5)
                LeadscrewStep(state=LeadscrewState.STASH_HEIGHT).run()
                DisableVacuumStep().run()
                LeadscrewStep(state=LeadscrewState.CLEAR_STASH_HEIGHT).run()

                # Elbow puck into place
                logger.info('Elbowing puck')
                ScaraMoveToStep(Coord(x=0, y=160)).run()
                LeadscrewStep(state=LeadscrewState.ELBOW_PUCK_HEIGHT).run()
                ScaraSpoonStep(side=self.side).run()

            except NoPuckError:
                self._num_no_puck_error += 1

                if self._num_no_puck_error < self._MAX_NO_PUCK_ERROR:
                    continue

                break
