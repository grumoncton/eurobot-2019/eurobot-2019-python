# Facteur de réduction de la résolution par rapport à (960, 540)
# Ex : N_ADJ = 1 donne (960, 540)
# Ex : N_ADJ = 2 donne (480, 270)
N_ADJ = 1


def adjust_x(vision_x):
    """
    Returns a scara X `sX` from a vision x `vX`, according to the rule

        `sX = k_x * vX + b_x`

    """

    # Nombre de mm / pixels
    k_x = 38 / 121 * N_ADJ

    # 0 = k_x * moyenne(deux points) + b_x
    b_x = - k_x * 486 / N_ADJ

    return k_x * vision_x + b_x


def adjust_y(vision_y):
    """
    Returns a scara Y `sY` from a vision Y `vY`, according to the rule

        `sY = k_y * vY + b_y`

    """

    # Nombre de mm/pixels + inversion du signe
    k_y = -38 / 121 * N_ADJ

    # 65 mm = k_y * pixel du centre des rondelles en y + b_y
    b_y = 65 - k_y * 307.5 / N_ADJ

    return k_y * vision_y + b_y


def adjust_center(contour, num_digits=0):
    """
    Adjust a center given in a tuple, consisting of (x, y) and round it with
    `num_digits`.

    """

    if not contour:
        return None

    return (
        round(adjust_x(contour[0]), num_digits),
        round(adjust_y(contour[1]), num_digits),
    )
