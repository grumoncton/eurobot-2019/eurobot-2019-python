from unittest.mock import MagicMock

from lib import dotenv

CARROT_PIN = 4
HAS_GPIO = dotenv.get_value('HAS_GPIO')


def get_GPIO():

    if not HAS_GPIO:
        return MagicMock()

    from RPi import GPIO  # pylint: disable=import-error

    GPIO.setmode(GPIO.BCM)

    GPIO.setup(CARROT_PIN, GPIO.IN)
    return GPIO
