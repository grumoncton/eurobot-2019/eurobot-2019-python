from lib import dotenv


ROBOT_NAME = str(dotenv.get_value('ROBOT_NAME'))


def get_i2c_devices():
    if ROBOT_NAME == 'primary':
        from devices.primary_devices import i2c_devices
        return i2c_devices

    if ROBOT_NAME == 'secondary':
        from devices.secondary_devices import i2c_devices
        return i2c_devices

    raise Exception('Fuck')


def get_disconnected_devices():
    """
    Check if each device is connected. Return list of dicsonnected devices.
    """

    disconnected_i2c_devices = [
        device for device in get_i2c_devices()
        if not device.is_connected()
    ]

    return disconnected_i2c_devices
