from enum import IntEnum


class PrimaryAcceleratorInstruction(IntEnum):
    TILTER_RAISE = 0x30
    TILTER_LOWER = 0x31

    STEPPER_SORT_RIGHT = 0x32
    STEPPER_SORT_LEFT = 0x33
