from devices import I2CStatusDevice


class PrimaryAccelerator(I2CStatusDevice):
    addr = 0x35
    name = 'PrimaryAccelerator'
