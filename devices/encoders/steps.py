from task_manager import BaseStep
from . import EncoderInstruction, encoders


class EncoderGetPositionStep(BaseStep):

    name = 'EncoderGetPositionStep'

    def __init__(self):
        self.device = encoders
        self.instruction = EncoderInstruction.ENCODER_GET_POSITION

        super().__init__()

    def run(self):
        return self.device.get_position()


class EncoderSetPositionStep(BaseStep):

    name = 'EncoderSetPositionStep'
    device = encoders

    def __init__(self, position):
        self.position = position
        self.instruction = EncoderInstruction.ENCODER_SET_POSITION

        super().__init__()

    def run(self):
        return self.device.set_position(position=self.position)
