import logging

from devices import I2CStatusDevice
from comms import read_data, send_instruction
from lib import Coord, TableSide, dotenv


logger = logging.getLogger(__name__)

TABLE_SIDE = TableSide(str(dotenv.get_value('TABLE_SIDE')))


class Encoders(I2CStatusDevice):
    addr = 0x42
    name = 'Encoders'

    def get_position(self):
        from . import EncoderInstruction

        response = read_data(
            device=self,
            instruction=EncoderInstruction.ENCODER_GET_POSITION,
        )

        position = Coord.from_bytes(response)

        if TABLE_SIDE == TableSide.PURPLE:
            position = position.from_prime()

        logger.debug('Got position from encoders: %(position)s.', {
            'position': position,
        })

        return position

    def set_position(self, position):
        from . import EncoderInstruction

        logger.debug('Setting position to encoders: %(position)s.', {
            'position': position,
        })

        if TABLE_SIDE == TableSide.PURPLE:
            position = position.to_prime()

        send_instruction(
            device=self,
            instruction=EncoderInstruction.ENCODER_SET_POSITION,
            args=position.to_bytes(),
        )
