from enum import IntEnum, unique


@unique
class EncoderInstruction(IntEnum):
    ENCODER_GET_POSITION = 0x30
    ENCODER_SET_POSITION = 0x31
