import time
import math
import logging
import threading
from enum import IntEnum

from lidar import lidar
from lib import Coord, RobotSide, TableSide, RotationDirection, \
    shortest_angle_to, dotenv
from devices.encoders import encoders

logger = logging.getLogger(__name__)
ODRIVE_SERIAL_NUMBER = dotenv.get_value('ODRIVE_SERIAL_NUMBER')
TABLE_SIDE = TableSide(str(dotenv.get_value('TABLE_SIDE')))


try:
    import odrive.enums
    from lib.odrive_connector import OdriveConnector
except ImportError:
    from unittest.mock import MagicMock

    logger.warning('Mocking odrive')
    odrive = MagicMock()
    OdriveConnector = MagicMock()


class Wheels:
    # pylint: disable=too-many-instance-attributes

    _PULLEY_RATIO = 40 / 16
    _COUNTS_PER_REVOLUTION = 2048

    _WHEEL_DIAMETER = 50
    _WHEEL_SEPARATION = 179.1
    _COUNTS_PER_MM = _COUNTS_PER_REVOLUTION * _PULLEY_RATIO / \
        (_WHEEL_DIAMETER * math.pi)
    _COUNTS_PER_RADIAN = (_WHEEL_SEPARATION / 2) * \
        _COUNTS_PER_MM

    # Speed (counts / s) below which robot is considered stopped.
    _STOP_TOLERANCE = 1
    _COUNT_TOLERANCE = 20

    # Seconds to take when braking
    _BRAKE_TIME = 0.3

    DRIVE_ACCEL = 35e3
    DRIVE_DECEL = 35e3

    BRAKE_DECEL = 70e3

    DRIVE_SPEED = 30e3

    class Direction(IntEnum):
        FORWARDS = 1
        BACKWARDS = -1

    class StopException(Exception):
        pass

    def __init__(self):
        self.pos = Coord(x=450, y=165, a=math.radians(118))

        encoders.set_position(position=self.pos)

        self.od = OdriveConnector(serial_number=ODRIVE_SERIAL_NUMBER) \
            .find_odrive()
        self.od.axis0.requested_state = odrive.enums.AXIS_STATE_IDLE
        self.od.axis0.requested_state = odrive.enums.AXIS_STATE_IDLE

        self._reset_config()

    @property
    def vbus_voltage(self):
        return self.od.vbus_voltage

    def _get_axis(self, side):
        """
        Get odrive axis based on robot side (`RobotSide.RIGHT` is only right on
        yelllow table side) and table side.
        """

        return self.od.axis0 \
            if (side == RobotSide.RIGHT and TABLE_SIDE == TableSide.YELLOW) \
            or (side == RobotSide.LEFT and TABLE_SIDE == TableSide.PURPLE) \
            else self.od.axis1

    @staticmethod
    def _get_axis_multiplier(side):
        """
        Since one motor is a mirror of the other, one wheel has to spin
        backwards to drive state. Each axis either has to be multiplied by 1 or
        -1. Get that multiplier.
        """

        return -1 \
            if (side == RobotSide.RIGHT and TABLE_SIDE == TableSide.YELLOW) \
            or (side == RobotSide.LEFT and TABLE_SIDE == TableSide.PURPLE) \
            else 1

    def _get_axis_counts(self, side):

        return self._get_axis(side).encoder.pos_estimate * \
            self._get_axis_multiplier(side)

    def configure_axis(
            self,
            side,
            speed=DRIVE_SPEED,
            accel=DRIVE_ACCEL,
            decel=DRIVE_DECEL,
    ):
        """
        Configure robot axis based on table side.
        """

        axis = self._get_axis(side=side)

        axis.trap_traj.config.vel_limit = speed
        axis.trap_traj.config.accel_limit = accel
        axis.trap_traj.config.decel_limit = decel

    def _reset_config(self):
        for side in RobotSide:
            self.configure_axis(
                side=side,
                speed=self.DRIVE_SPEED,
                accel=self.DRIVE_ACCEL,
                decel=self.DRIVE_ACCEL,
            )

    def change_config(self):
        """
        Get context manager to reset config on exit.
        """

        from lib import ContextManager

        return ContextManager(on_exit=self._reset_config)

    def engage(self):
        """
        Enter closed loop control.
        """

        from lib import ContextManager

        assert self.od.axis0.current_state == odrive.enums.AXIS_STATE_IDLE and \
            self.od.axis1.current_state == odrive.enums.AXIS_STATE_IDLE, \
            'Axes not in idle state'

        self.od.axis0.requested_state = \
            odrive.enums.AXIS_STATE_CLOSED_LOOP_CONTROL
        self.od.axis1.requested_state = \
            odrive.enums.AXIS_STATE_CLOSED_LOOP_CONTROL

        return ContextManager(on_exit=self.disengage)

    def disengage(self):
        """
        Enter idle state.
        """

        self.od.axis0.requested_state = odrive.enums.AXIS_STATE_IDLE
        self.od.axis1.requested_state = odrive.enums.AXIS_STATE_IDLE

    def _brake_axis(self, axis):
        axis.trap_traj.config.decel_limit = self.BRAKE_DECEL
        axis.controller.move_incremental(
            axis.encoder.vel_estimate * self._BRAKE_TIME,
            False,
        )

    def stop(self):
        """
        Stop moving as soon as possible.
        """

        with self.change_config():
            self._brake_axis(axis=self.od.axis0)
            self._brake_axis(axis=self.od.axis1)

            lidar.stop_event.clear()

            # Wait until stopped
            while max(
                    self.od.axis0.encoder.vel_estimate,
                    self.od.axis1.encoder.vel_estimate,
            ) > self._STOP_TOLERANCE:
                time.sleep(0.1)

        raise self.StopException()

    def _wait_until_dest_reached(self, axis, counts):
        """
        Periodically check if axis has done given counts. Also check stop event.
        """

        from time_up_handler import time_up_event

        i = 0
        initial_counts = axis.encoder.pos_estimate

        while True:

            # Check distance 80 times less often than stop event
            # Return if close to destination
            if i > 80:
                if abs(axis.encoder.pos_estimate - initial_counts - counts) < \
                        self._COUNT_TOLERANCE:
                    return

                i = 0

            if lidar.stop_event.is_set():
                self.stop()

            if time_up_event.is_set():
                try:
                    self.stop()
                finally:
                    time.sleep(1000000)

            i += 1
            time.sleep(1e-3)

    def _move_wheel(self, side, counts, queue):
        """
        Move axis given counts and block until destination reached.
        """

        if counts == 0:
            return

        axis = self._get_axis(side=side)
        counts *= self._get_axis_multiplier(side=side)

        try:
            axis.controller.move_incremental(counts, False)

            self._wait_until_dest_reached(axis=axis, counts=counts)

        except Exception as e:  # pylint: disable=broad-except
            queue.put(e)

    def _move_wheels(self, right_counts, left_counts):
        import queue

        exception_queue = queue.Queue()

        # Get initial counts for each axis
        # Used to recover from a sudden stop
        initial_right_counts = self._get_axis_counts(side=RobotSide.RIGHT)
        initial_left_counts = self._get_axis_counts(side=RobotSide.LEFT)

        try:

            # Run each wheel in thread
            threads = [threading.Thread(
                daemon=True,
                target=self._move_wheel,
                kwargs={
                    'side': side,
                    'counts': counts,
                    'queue': exception_queue,
                },
            ) for side, counts in {
                RobotSide.RIGHT: right_counts,
                RobotSide.LEFT: left_counts,
            }.items()]

            for thread in threads:
                thread.start()

            # Propagate exceptions from queue
            while True:
                time.sleep(0.01)

                try:
                    exception = exception_queue.get_nowait()
                    raise exception
                except queue.Empty:
                    pass

                if not any(thread.is_alive() for thread in threads):
                    break

            for thread in threads:
                thread.join()

        # Recover from sudden stop
        except self.StopException:
            while lidar.is_blocked:
                logger.info('Blocked by obstacle. Sleeping.')
                time.sleep(0.5)

            logger.info('Unblocked. Attempting to move remaining counts.')

            remaining_right_counts = initial_right_counts - \
                self._get_axis_counts(side=RobotSide.RIGHT) + right_counts

            remaining_left_counts = initial_left_counts - \
                self._get_axis_counts(side=RobotSide.LEFT) + left_counts

            logger.debug(
                'Remiaining right counts: %f. Remaining left counts %f.',
                remaining_right_counts,
                remaining_left_counts,
            )

            with self.change_config():
                for axis in (self.od.axis0, self.od.axis1):
                    axis.trap_traj.config.vel_limit = \
                        axis.trap_traj.config.vel_limit / 2
                    axis.trap_traj.config.accel_limit = \
                        axis.trap_traj.config.accel_limit / 2
                    axis.trap_traj.config.decel_limit = \
                        axis.trap_traj.config.decel_limit / 2

                    axis.requested_state = odrive.enums.AXIS_STATE_IDLE
                    axis.requested_state = \
                        odrive.enums.AXIS_STATE_CLOSED_LOOP_CONTROL

                lidar.stop_event.clear()
                self._move_wheels(
                    right_counts=remaining_right_counts,
                    left_counts=remaining_left_counts,
                )

    def rotate_to(self, heading):
        if abs(heading) > math.pi * 2:
            print('Heading pretty big. Did you forget to convert to radians?')
            raise RuntimeError()

        logger.debug('Rotating to %(heading)f.', {
            'heading': math.degrees(heading),
        })

        delta = shortest_angle_to(
            current=self.pos.a,
            target=heading,
        )
        direction = RotationDirection(math.copysign(1, delta))

        logger.debug(
            'Angle delta: %f. Direction: %s.',
            math.degrees(delta),
            str(direction),
        )

        counts = abs(delta * self._COUNTS_PER_RADIAN)

        left_counts = counts
        right_counts = counts

        if direction == RotationDirection.CCW:
            left_counts = -counts
        else:
            right_counts = -counts

        logger.debug(
            'Right counts: %f. Left counts: %f.',
            right_counts,
            left_counts,
        )

        self._move_wheels(right_counts=right_counts, left_counts=left_counts)

        self.pos = encoders.get_position()

    def rotate_around_one_wheel(self, heading, side):
        if abs(heading) > math.pi * 2:
            print('Heading pretty big. Did you forget to convert to radians?')
            raise RuntimeError()

        logger.debug('Rotating around 1 wheel to %(heading)f.', {
            'heading': math.degrees(heading),
        })

        delta = shortest_angle_to(
            current=self.pos.a,
            target=heading,
        )
        direction = RotationDirection(math.copysign(1, delta))

        logger.debug(
            'Angle delta: %f. Direction: %s.',
            math.degrees(delta),
            str(direction),
        )

        counts = abs(delta * self._COUNTS_PER_RADIAN * 2 * side)

        left_counts = 0
        right_counts = 0

        if side == RobotSide.RIGHT:
            right_counts = counts
        else:
            left_counts = counts

        logger.debug(
            'Right counts: %f. Left counts: %f.',
            right_counts,
            left_counts,
        )

        self._move_wheels(right_counts=right_counts, left_counts=left_counts)

        self.pos = encoders.get_position()

    def drive_straight(self, distance, direction=Direction.FORWARDS):
        counts = distance * direction * self._COUNTS_PER_MM

        self._move_wheels(right_counts=counts, left_counts=counts)

        self.pos = encoders.get_position()

    def move_to(self, destination, direction=None):
        direction = direction or self.Direction.FORWARDS

        lidar.set_angle(
            0 if direction == self.Direction.FORWARDS else 180,
        )

        logger.info('Moving to destination %s.', str(destination))

        self.pos = encoders.get_position()

        delta = destination - self.pos
        heading = self.pos.angle_to(destination)

        if direction == self.Direction.BACKWARDS:
            heading += math.pi

        logger.debug(
            'Delta: %s. Heading: %f.',
            str(delta),
            math.degrees(heading),
        )

        logger.debug('Rotating to target heading.')
        self.rotate_to(heading=heading)

        logger.debug('Driving straight for %d mm.', delta.hypot())
        self.drive_straight(delta.hypot(), direction=direction)

        if destination.a:
            logger.debug('Rotating to destination angle.')
            self.rotate_to(heading=destination.a)

    def move_to_relative(self, destination, *args, **kwargs):
        return self.move_to(
            destination=self.pos + destination,
            *args,
            **kwargs,
        )

    def rotate_around_point(self, heading, side, radius):
        CURVILIGNE_SPEED = 30e3
        CURVILIGNE_ACCEL = 20e3
        CURVILIGNE_DECEL = 20e3

        if abs(heading) > math.pi * 2:
            print('Heading pretty big. Did you forget to convert to radians?')
            raise RuntimeError()

        logger.debug(
            'Rotating around point with radius %(radius)f until heading '
            '%(heading)f.',
            {
                'radius': radius,
                'heading': math.degrees(heading),
            },
        )

        delta = shortest_angle_to(
            current=self.pos.a,
            target=heading,
        )
        direction = RotationDirection(math.copysign(1, delta))

        logger.debug(
            'Angle delta: %f. Direction: %s.',
            math.degrees(delta),
            str(direction),
        )

        outside_counts = abs(delta * (radius + self._WHEEL_SEPARATION / 2) *
                             self._COUNTS_PER_MM * side)
        inside_counts = abs(delta * (radius - self._WHEEL_SEPARATION / 2) *
                            self._COUNTS_PER_MM * side)

        def set_outside_config(axis):
            ratio = (radius + self._WHEEL_SEPARATION / 2) / radius
            axis.trap_traj.config.vel_limit = CURVILIGNE_SPEED * ratio
            axis.trap_traj.config.accel_limit = CURVILIGNE_ACCEL * ratio
            axis.trap_traj.config.decel_limit = CURVILIGNE_DECEL * ratio

        def set_inside_config(axis):
            ratio = (radius - self._WHEEL_SEPARATION / 2) / radius
            axis.trap_traj.config.vel_limit = CURVILIGNE_SPEED * ratio
            axis.trap_traj.config.accel_limit = CURVILIGNE_ACCEL * ratio
            axis.trap_traj.config.decel_limit = CURVILIGNE_DECEL * ratio

        with self.change_config():
            if side == RobotSide.RIGHT:
                right_counts = outside_counts
                left_counts = inside_counts
                set_outside_config(self._get_axis(side=RobotSide.RIGHT))
                set_inside_config(self._get_axis(side=RobotSide.LEFT))

            else:
                right_counts = inside_counts
                left_counts = outside_counts
                set_outside_config(self._get_axis(side=RobotSide.LEFT))
                set_inside_config(self._get_axis(side=RobotSide.RIGHT))

            logger.debug(
                'Right counts: %f. Left counts: %f.',
                right_counts,
                left_counts,
            )

            self._move_wheels(
                right_counts=right_counts,
                left_counts=left_counts,
            )

            self.pos = encoders.get_position()
