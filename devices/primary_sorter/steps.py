import struct

from lib import dotenv, TableSide
from task_manager import BaseStep
from comms import send_instruction


TABLE_SIDE = TableSide(str(dotenv.get_value('TABLE_SIDE')))


class RaiseFlipperStep(BaseStep):
    """
    Raise flipper.
    """

    name = 'RaiseFlipperStep'

    def __init__(self, side):

        self.side = side

        super().__init__()

    def run(self):
        from . import primary_sorter_device, PrimarySorterInstruction

        send_instruction(
            device=primary_sorter_device,
            instruction=PrimarySorterInstruction.RAISE_FLIPPER,
            args=struct.pack('b', self.side.value),
        )


class MidFlipperStep(BaseStep):
    """
    Mid flipper
    """

    name = 'MidFlipperStep'

    def __init__(self, side):

        self.side = side

        super().__init__()

    def run(self):
        from . import primary_sorter_device, PrimarySorterInstruction

        send_instruction(
            device=primary_sorter_device,
            instruction=PrimarySorterInstruction.MID_FLIPPER,
            args=struct.pack('b', self.side.value),
        )


class LowerFlipperStep(BaseStep):
    """
    Lower flipper
    """

    name = 'LowerFlipperStep'

    def __init__(self, side):

        self.side = side

        super().__init__()

    def run(self):
        from . import primary_sorter_device, PrimarySorterInstruction

        send_instruction(
            device=primary_sorter_device,
            instruction=PrimarySorterInstruction.LOWER_FLIPPER,
            args=struct.pack('b', self.side.value),
        )


class RaiseSlideBlockerStep(BaseStep):
    """
    Raise slide blocker
    """

    name = 'RaiseSlideBlockerStep'

    def __init__(self, side):

        self.side = side

        super().__init__()

    def run(self):
        from . import primary_sorter_device, PrimarySorterInstruction

        send_instruction(
            device=primary_sorter_device,
            instruction=PrimarySorterInstruction.RAISE_SLIDE_BLOCKER,
            args=struct.pack('b', self.side.value),
        )


class LowerSlideBlockerStep(BaseStep):
    """
    Lower slide blocker
    """

    name = 'LowerSlideBlockerStep'

    def __init__(self, side):

        self.side = side

        super().__init__()

    def run(self):
        from . import primary_sorter_device, PrimarySorterInstruction

        send_instruction(
            device=primary_sorter_device,
            instruction=PrimarySorterInstruction.LOWER_SLIDE_BLOCKER,
            args=struct.pack('b', self.side.value),
        )


class SetSortersStep(BaseStep):

    name = 'SetSortersStep'

    def __init__(self, state):
        """
        Set all sorter servos to the given state. Each relay gets one bit in
        the given byte.
        """

        self.state = state
        super().__init__()

    def run(self):
        from . import primary_sorter_device, PrimarySorterInstruction

        state = self.state

        if TABLE_SIDE == TableSide.PURPLE:
            state &= 0b010010
            state |= ((0b001001 & self.state) << 2)
            state |= ((0b100100 & self.state) >> 2)

        send_instruction(
            device=primary_sorter_device,
            instruction=PrimarySorterInstruction.SET_SORTERS,
            args=[state],
        )
