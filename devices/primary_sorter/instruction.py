from enum import IntEnum


class PrimarySorterInstruction(IntEnum):
    SET_SORTERS = 0x30
    RAISE_FLIPPER = 0x31
    LOWER_FLIPPER = 0x32
    RAISE_SLIDE_BLOCKER = 0x33
    LOWER_SLIDE_BLOCKER = 0x34
    MID_FLIPPER = 0x35
