from task_manager import BaseStep


class LeadscrewStep(BaseStep):

    name = 'LeadscrewStep'

    def __init__(self, state):
        from .. import secondary_mechanisms_device

        self.state = state
        self.device = secondary_mechanisms_device

        super().__init__()

    def run(self):
        from comms import send_instruction
        from .. import SecondaryMechanismsInstruction

        send_instruction(
            device=self.device,
            instruction=SecondaryMechanismsInstruction.LEADSCREW_MOVE_TO,
            args=[self.state.value],
        )

        self.wait_until_ready_state()
