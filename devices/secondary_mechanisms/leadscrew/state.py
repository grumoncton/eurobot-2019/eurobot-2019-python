from enum import IntEnum


class LeadscrewState(IntEnum):
    HOME = 0
    PUCK_PICK_UP_HEIGHT = 1
    STASH_HEIGHT = 2
    CLEAR_STASH_HEIGHT = 3
    ELBOW_PUCK_HEIGHT = 4
