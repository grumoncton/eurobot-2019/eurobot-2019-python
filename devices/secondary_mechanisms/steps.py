from task_manager import SimpleStep
from . import SecondaryMechanismsInstruction
from .secondary_mechanisms import secondary_mechanisms_device


class EnableVacuumStep(SimpleStep):
    device = secondary_mechanisms_device
    instruction = SecondaryMechanismsInstruction.ENABLE_VACUUM


class DisableVacuumStep(SimpleStep):
    device = secondary_mechanisms_device
    instruction = SecondaryMechanismsInstruction.DISABLE_VACUUM


class OpenPlowStep(SimpleStep):
    device = secondary_mechanisms_device
    instruction = SecondaryMechanismsInstruction.OPEN_PLOW


class ClosePlowStep(SimpleStep):
    device = secondary_mechanisms_device
    instruction = SecondaryMechanismsInstruction.CLOSE_PLOW


class CradlePlowStep(SimpleStep):
    device = secondary_mechanisms_device
    instruction = SecondaryMechanismsInstruction.CRADLE_PLOW


class SplitPlowStep(SimpleStep):
    device = secondary_mechanisms_device
    instruction = SecondaryMechanismsInstruction.SPLIT_PLOW


class StraightPlowStep(SimpleStep):
    device = secondary_mechanisms_device
    instruction = SecondaryMechanismsInstruction.STRAIGHT_PLOW
