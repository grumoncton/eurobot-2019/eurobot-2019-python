from enum import IntEnum


class RangeFinderInstruction(IntEnum):
    SET_ADDRESS = 0x8A
