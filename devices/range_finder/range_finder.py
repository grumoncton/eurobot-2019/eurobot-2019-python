import logging

from comms import i2c
from devices.i2c_device import I2CDevice


logger = logging.getLogger(__name__)


try:
    import VL53L0X  # pylint: disable=import-error
except ImportError:
    from unittest.mock import MagicMock

    logger.warning('Mocking VL53L0X.')
    VL53L0X = MagicMock()


class RangeFinder(I2CDevice):
    def __init__(self, addr, pin, *args, **kwargs):
        self._addr = addr
        self.pin = pin

        self._tof = VL53L0X.VL53L0X(i2c_address=addr)
        self.accuracy = VL53L0X.Vl53l0xAccuracyMode.BEST

        super().__init__(*args, **kwargs)

    @property
    def addr(self):
        return self._addr

    @addr.setter
    def addr(self, addr):
        """
        tof needs address, so update it with setter.
        """

        self._addr = addr
        self._tof.i2c_address = addr

    def is_connected(self):
        try:
            i2c.write_quick(self.addr)
            return True
        except OSError as e:
            print(e)
            return False

    def open(self):
        """
        Open connection to tof device.
        """

        return self._tof.open()

    def close(self):
        """
        Close connection to tof device.
        """

        return self._tof.close()

    def start_ranging(self):
        """
        Start ranging with predefined accuracy mode.
        """

        return self._tof.start_ranging(self.accuracy)

    def stop_ranging(self):
        """
        Stop ranging.
        """

        return self._tof.stop_ranging()

    def _read_with_delay(self, delay):
        """
        Read range finder, wait given delay (usually corresponds to range
        finder timing), then return distance.
        """

        import time

        distance = self._tof.get_distance()
        time.sleep(delay)
        return distance

    def read_distance(self, num_samples=5):
        """
        Sample rannge finder multples times to get a mean reading.
        """

        try:
            self.open()
            self.start_ranging()

            timing = self._tof.get_timing()

            # Read range finder multiple times
            distances = [self._read_with_delay(timing / 1e6)
                         for _ in range(num_samples)]

        finally:
            self.stop_ranging()
            self.close()

        return sum(distances) / num_samples

    def __str__(self):
        return 'RangeFinder(pin={pin}, addr={addr})'.format(
            pin=self.pin,
            addr=self.addr,
        )

    def __repr__(self):
        return self.__str__()
