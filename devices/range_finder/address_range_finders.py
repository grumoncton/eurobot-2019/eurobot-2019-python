import time
import logging

from comms import i2c


logger = logging.getLogger(__name__)

IO_EXPANDER_ADDRESS = 0x20
DEFAULT_RANGE_FINDER_ADDRESS = 0x29
DELAY = 0.1


def _recurse(remaining_range_finders, io_expander_state):
    from . import RangeFinderInstruction

    if not remaining_range_finders:
        return

    logger.debug('Setting range finder')
    range_finder = remaining_range_finders[0]

    io_expander_state |= 1 << range_finder.pin

    logger.debug('Writing to expander: %(state)s.', {
        'state': bin(io_expander_state),
    })
    i2c.write_byte(IO_EXPANDER_ADDRESS, io_expander_state)

    time.sleep(DELAY)

    logger.debug('Writing setting range finder address to 0x%(addr)02x', {
        'addr': range_finder.addr,
    })

    try:
        i2c.write_byte_data(
            DEFAULT_RANGE_FINDER_ADDRESS,
            RangeFinderInstruction.SET_ADDRESS,
            range_finder.addr,
        )
    except OSError:
        pass

    time.sleep(DELAY)

    _recurse(
        remaining_range_finders=remaining_range_finders[1:],
        io_expander_state=io_expander_state,
    )


def address_range_finders(range_finders):
    """
    Assign each range finder a unique address by iterating through them,
    shutting the rest down, then sending it its new address.
    """

    # Shutdown all range finders
    i2c.write_byte(IO_EXPANDER_ADDRESS, 0)

    _recurse(remaining_range_finders=range_finders, io_expander_state=0x00)
