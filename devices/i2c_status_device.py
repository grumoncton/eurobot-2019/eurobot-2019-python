from comms import read_data, BaseInstruction, CommsStatus
from .i2c_device import I2CDevice


class I2CStatusDevice(I2CDevice):
    def is_connected(self):
        try:
            status = read_data(
                device=self,
                instruction=BaseInstruction.GET_STATUS,
            )[0]

            return status == CommsStatus.READY

        except OSError:
            return False
