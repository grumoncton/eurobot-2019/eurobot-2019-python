import time
import math
import logging
import itertools
import threading
from collections import OrderedDict
import numpy as np

from lcd import lcd
from lidar import lidar
from lib import Coord, RobotSide, TableSide, RobotFace, RotationDirection, \
    shortest_angle_to, dotenv
from .jacobian import compute_jacobian


logger = logging.getLogger(__name__)
FRONT_ODRIVE_SERIAL_NUMBER = dotenv.get_value('FRONT_ODRIVE_SERIAL_NUMBER')
BACK_ODRIVE_SERIAL_NUMBER = dotenv.get_value('BACK_ODRIVE_SERIAL_NUMBER')
TABLE_SIDE = TableSide(str(dotenv.get_value('TABLE_SIDE')))


try:
    import odrive.enums
    from lib.odrive_connector import OdriveConnector
except ImportError:
    from unittest.mock import MagicMock

    logger.warning('Mocking odrive')
    odrive = MagicMock()
    OdriveConnector = MagicMock()


class Mecanum:
    _PULLEY_RATIO = 40 / 16
    _COUNTS_PER_REVOLUTION = 2048

    _WHEEL_DIAMETER = 59
    _WHEEL_RADIUS = _WHEEL_DIAMETER / 2
    _WHEEL_SEPARATION = 158.5

    _COUNTS_PER_MM = (_COUNTS_PER_REVOLUTION * _PULLEY_RATIO) / \
        (math.pi * _WHEEL_DIAMETER)

    _ROTATION_SPEED = 4e3
    _ROTATION_ACCEL = 2e3
    _ROTATION_DECEL = 2e3

    _DRIVE_SPEED = 400
    _DRIVE_ACCEL_DISTANCE = 500

    _BRAKE_DIST = 200

    _STOP_TOLERANCE = 2

    class StopException(Exception):
        pass

    def __init__(self):
        # TODO
        self.pos = Coord(x=770, y=225, a=math.radians(180))

        od = {
            RobotFace.FRONT: OdriveConnector(
                serial_number=FRONT_ODRIVE_SERIAL_NUMBER,
            ).find_odrive(),
            RobotFace.BACK: OdriveConnector(
                serial_number=BACK_ODRIVE_SERIAL_NUMBER,
            ).find_odrive(),
        }
        self.od = od

        lcd.display_string(
            'Voltage: %.2fV' % self.od[RobotFace.FRONT].vbus_voltage,
            line=2,
        )

        self._jacobian = compute_jacobian(
            wheel_separation=self._WHEEL_SEPARATION,
        )

        # List of axes ids in the same order that they are used in the jacobian
        self._axes = OrderedDict([
            ((RobotSide.LEFT, RobotFace.FRONT), od[RobotFace.FRONT].axis1),
            ((RobotSide.RIGHT, RobotFace.FRONT), od[RobotFace.FRONT].axis0),
            ((RobotSide.LEFT, RobotFace.BACK), od[RobotFace.BACK].axis1),
            ((RobotSide.RIGHT, RobotFace.BACK), od[RobotFace.BACK].axis0),
        ])

        for axis in self._axes.values():
            axis.requested_state = odrive.enums.AXIS_STATE_IDLE

        self._reset_config()

        self._is_braking = False

    @property
    def vbus_voltage(self):
        return self.od[RobotFace.FRONT].vbus_voltage

    @staticmethod
    def _get_axis_multiplier():
        return 1 if TABLE_SIDE == TableSide.YELLOW else -1

    @staticmethod
    def _get_rotation_axis_multiplier(side):
        """
        Since one motor is a mirror of the other, one wheel has to spin
        backwards to drive state. Each axis either has to be multiplied by 1 or
        -1. Get that multiplier.
        """

        return 1 if side == RobotSide.LEFT else -1

    def engage(self):
        """
        Enter closed loop control.
        """

        from lib import ContextManager

        for axis in self._axes.values():
            # assert axis.current_state == odrive.enums.AXIS_STATE_IDLE, \
            #     'Axes not in idle state'

            axis.requested_state = odrive.enums.AXIS_STATE_CLOSED_LOOP_CONTROL

        return ContextManager(on_exit=self.disengage)

    def disengage(self):
        """
        Enter idle state.
        """

        for axis in self._axes.values():
            axis.requested_state = odrive.enums.AXIS_STATE_IDLE

    def _reset_config(self):
        for axis in self._axes.values():
            axis.trap_traj.config.vel_limit = self._ROTATION_SPEED
            axis.trap_traj.config.accel_limit = self._ROTATION_ACCEL
            axis.trap_traj.config.decel_limit = self._ROTATION_DECEL

    def change_config(self):
        """
        Get context manager to reset config on exit.
        """

        from lib import ContextManager

        return ContextManager(on_exit=self._reset_config)

    def _wait_until_dest_reached(self, axis):
        """
        Periodically check if axis has done given counts. Also check stop event.
        """

        from time_up_handler import time_up_event

        i = 0

        while True:

            # Check distance 80 times less often than stop event
            # Return if close to destination
            if i > 80:
                if abs(axis.encoder.vel_estimate) < self._STOP_TOLERANCE:
                    return

                i = 0

            if lidar.stop_event.is_set():
                self.brake()

            if time_up_event.is_set():
                try:
                    self.brake()
                finally:
                    time.sleep(1000000)

            i += 1
            time.sleep(1e-3)

    def _move_wheel(self, side, face, counts, queue):
        """
        Move axis given by side and face by counts and block until destination
        reached.
        """

        axis = self._axes[(side, face)]
        counts *= self._get_axis_multiplier()

        try:
            axis.controller.move_incremental(counts, False)
            self._wait_until_dest_reached(axis=axis)

        except Exception as e:  # pylint: disable=broad-except
            queue.put(e)

    def _move_wheels(self, counts):
        """
        Move each of four wheels by given counts. Block until destination
        reached.
        """

        import queue

        exception_queue = queue.Queue()

        # Run each wheel in thread
        threads = [threading.Thread(
            daemon=True,
            target=self._move_wheel,
            kwargs={
                'side': side,
                'face': face,
                'counts': counts[(side, face)],
                'queue': exception_queue,
            },
        ) for side, face in itertools.product(RobotSide, RobotFace)]

        for thread in threads:
            thread.start()

        # Propagate exceptions from queue
        while True:
            time.sleep(0.01)

            try:
                exception = exception_queue.get_nowait()
                raise exception
            except queue.Empty:
                pass

            if not any(thread.is_alive() for thread in threads):
                break

        for thread in threads:
            thread.join()

    def brake(self):

        if self._is_braking:
            return

        self._is_braking = True

        lidar.stop_event.clear()

        decel_limit_per_wheel = [
            int(axis.trap_traj.config.decel_limit *
                (self._DRIVE_ACCEL_DISTANCE / self._BRAKE_DIST))
            for axis in self._axes.values()
        ]
        print('decel_limit_per_wheel', decel_limit_per_wheel)

        for axis, decel_limit in zip(
                self._axes.values(),
                decel_limit_per_wheel,
        ):
            axis.controller.vel_ramp_enable = True
            axis.controller.config.vel_ramp_rate = decel_limit
            axis.controller.vel_ramp_target = 0
            axis.controller.config.control_mode = \
                odrive.enums.CTRL_MODE_VELOCITY_CONTROL

        time.sleep(1)

        # Wait until stopped
        while True:
            time.sleep(0.01)

            for axis in self._axes.values():
                print(axis.encoder.vel_estimate)

            if any(
                    True for axis in self._axes.values()
                    if abs(axis.encoder.vel_estimate) > self._STOP_TOLERANCE
            ):
                continue

            break

        print('Stopped. Returning to position control')

        time.sleep(1)

        for axis in self._axes.values():
            axis.requested_state = odrive.enums.AXIS_STATE_IDLE
            axis.controller.config.control_mode = \
                odrive.enums.CTRL_MODE_POSITION_CONTROL
            axis.requested_state = odrive.enums.AXIS_STATE_CLOSED_LOOP_CONTROL

        print('done brake')

        self._is_braking = False

        raise self.StopException()

    def rotate_to(self, heading):
        if abs(heading) > math.pi * 2:
            print('Heading pretty big. Did you forget to convert to radians?')
            raise RuntimeError()

        logger.debug('Rotating to %(heading)f.', {
            'heading': math.degrees(heading),
        })

        delta = shortest_angle_to(
            current=self.pos.a,
            target=heading,
        )
        direction = RotationDirection(math.copysign(1, delta))

        logger.debug(
            'Angle delta: %f. Direction: %s.',
            math.degrees(delta),
            str(direction),
        )

        counts = abs(
            self._COUNTS_PER_MM * delta * self._WHEEL_SEPARATION / 0.707,
        )

        left_counts = counts * \
            self._get_rotation_axis_multiplier(side=RobotSide.LEFT)
        right_counts = counts * \
            self._get_rotation_axis_multiplier(side=RobotSide.RIGHT)

        if direction == RotationDirection.CCW:
            left_counts *= -1
        else:
            right_counts *= -1

        logger.debug(
            'Right counts: %f. Left counts: %f.',
            right_counts,
            left_counts,
        )

        self._move_wheels(counts={
            (RobotSide.RIGHT, RobotFace.FRONT): right_counts,
            (RobotSide.RIGHT, RobotFace.BACK): right_counts,
            (RobotSide.LEFT, RobotFace.FRONT): left_counts,
            (RobotSide.LEFT, RobotFace.BACK): left_counts,
        })

        self.pos.a = heading

    def move_to(self, destination, speed=_DRIVE_SPEED):
        def normalise_np_array(array):
            return abs(array).astype(int).flatten()

        delta = destination - self.pos
        absolute_angle = self.pos.angle_to(destination)
        relative_angle = absolute_angle - self.pos.a

        lidar.set_angle(angle=math.degrees(relative_angle))

        logger.debug('Moving to %s.', destination)

        accel_time = (2 * self._DRIVE_ACCEL_DISTANCE) / speed
        coast_time = 2 * (delta.hypot() - 2 * self._DRIVE_ACCEL_DISTANCE) / \
            speed

        x_prime_speed = speed * math.cos(relative_angle)
        y_prime_speed = speed * math.sin(relative_angle)

        logger.debug('X speed: %f, Y speed: %f.', x_prime_speed, y_prime_speed)

        speed_array = np.array([[x_prime_speed], [y_prime_speed], [0]])

        wheel_speeds = (-1 / self._WHEEL_RADIUS) * \
            self._jacobian.dot(speed_array) * \
            self._COUNTS_PER_MM * self._WHEEL_RADIUS

        wheel_accels = (wheel_speeds / accel_time)
        accel_counts = accel_time * wheel_speeds / 2
        coast_counts = coast_time * wheel_speeds / 2

        counts_per_wheel = -(2 * accel_counts + coast_counts).astype(int)

        wheel_speeds = normalise_np_array(wheel_speeds)
        wheel_accels = normalise_np_array(wheel_accels)

        for wheel_speed, wheel_accel, (side, face) in zip(
                wheel_speeds,
                wheel_accels,
                self._axes.keys(),
        ):
            axis = self._axes[(side, face)]
            axis.trap_traj.config.vel_limit = wheel_speed
            axis.trap_traj.config.accel_limit = wheel_accel
            axis.trap_traj.config.decel_limit = wheel_accel

        initial_counts_per_wheel = [
            axis.encoder.pos_estimate
            for axis in self._axes.values()
        ]

        try:
            self._move_wheels(counts=dict(zip(
                self._axes.keys(),
                counts_per_wheel,
            )))

            self.pos = self.pos.merge_with(destination)

        except self.StopException:
            while lidar.is_blocked:
                logger.info('Blocked by obstacle. Sleeping.')
                time.sleep(0.5)

            lidar.stop_event.clear()

            logger.info('Unblocked. Attempting to move to final destination.')

            time.sleep(1)

            distances = [
                int(delta.hypot() *
                    (axis.encoder.pos_estimate - initial_counts) / counts)
                for axis, counts, initial_counts in zip(
                    self._axes.values(),
                    counts_per_wheel,
                    initial_counts_per_wheel,
                )
            ]

            # TODO: Figure out angle drift
            distance = sum(distances) / len(distances)

            self.pos.x += distance * math.cos(absolute_angle)
            self.pos.y += distance * math.sin(absolute_angle)

            self.move_to(destination=destination, speed=speed)

    def move_to_relative(self, destination, speed=_DRIVE_SPEED):
        self.move_to(
            destination=self.pos + Coord(x=-destination.y, y=destination.x),
            speed=speed,
        )
