import math
import numpy as np


def compute_jacobian(wheel_separation):
    ALPHA = math.radians(57.7)
    BETA = math.radians(90)
    GAMMA = math.radians(45)

    a_array = [ALPHA, -ALPHA, math.pi - ALPHA, -(math.pi - ALPHA)]
    b_array = [BETA, -BETA, BETA, -BETA]
    g_array = [-GAMMA, GAMMA, GAMMA, -GAMMA]

    jacobian = np.array([
        [
            math.cos(b - g) / math.sin(g),
            math.sin(b - g) / math.sin(g),
            (wheel_separation * math.sin(b - g - a)) / math.sin(g)
        ] for a, b, g in zip(a_array, b_array, g_array)
    ])

    return jacobian
