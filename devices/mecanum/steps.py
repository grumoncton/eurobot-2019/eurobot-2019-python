from task_manager import BaseStep


class MoveToStep(BaseStep):
    """
    Move the mecanum drive to destination coordintaes.
    """

    name = 'MoveToStep'

    def __init__(self, destination):
        self.destination = destination

        super().__init__()

    def run(self):
        from devices.primary_devices import mecanum

        mecanum.move_to(destination=self.destination)


class MoveToRelativeStep(BaseStep):
    """
    Move the mecanum drive relative to its current position.
    """

    name = 'MoveToRelativeStep'

    def __init__(self, destination):
        self.destination = destination

        super().__init__()

    def run(self):
        from devices.primary_devices import mecanum

        mecanum.move_to_relative(destination=self.destination)


class RotateToStep(BaseStep):
    """
    Rotate the mecanum drive to the given heading.
    """

    name = 'RotateToStep'

    def __init__(self, heading):
        self.heading = heading

        super().__init__()

    def run(self):
        from devices.primary_devices import mecanum

        mecanum.rotate_to(heading=self.heading)
