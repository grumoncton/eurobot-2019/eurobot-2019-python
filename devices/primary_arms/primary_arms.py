from devices import I2CStatusDevice


class PrimaryArms(I2CStatusDevice):
    addr = 0x43
    name = 'PrimaryArms'


primary_arms_device = PrimaryArms()
