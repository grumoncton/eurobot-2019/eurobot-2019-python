import struct
from enum import IntEnum

from comms import send_instruction
from task_manager import BaseStep, SimpleStep, SimpleReadyStateStep
from .primary_arms import primary_arms_device
from .instruction import PrimaryArmsInstruction


class HomeInGameStep(SimpleReadyStateStep):

    device = primary_arms_device
    instruction = PrimaryArmsInstruction.HOME_IN_GAME


class LeadscrewExtendStep(SimpleReadyStateStep):
    """
    Extend leadscrew on main arm.
    """

    device = primary_arms_device
    instruction = PrimaryArmsInstruction.LEADSCREW_EXTEND


class LeadscrewRetractStep(SimpleReadyStateStep):
    """
    Retract leadscrew on main arm.
    """

    device = primary_arms_device
    instruction = PrimaryArmsInstruction.LEADSCREW_RETRACT


class ActivateSolenoidsStep(SimpleStep):
    """
    Activate solenoids, switching suction from the main arm suckers to the gold
    arm suckers.
    """

    device = primary_arms_device
    instruction = PrimaryArmsInstruction.ACTIVATE_SOLENOIDS


class DeactivateSolenoidsStep(SimpleStep):
    """
    Deactivate solenoids, switching suction from the gold arm suckers to the
    main arm suckers.
    """

    device = primary_arms_device
    instruction = PrimaryArmsInstruction.DEACTIVATE_SOLENOIDS


class RaiseMainArmStep(SimpleStep):
    """
    Raise main arm.
    """

    device = primary_arms_device
    instruction = PrimaryArmsInstruction.RAISE_MAIN_ARM


class LowerMainArmStep(SimpleStep):
    """
    Lower main arm.
    """

    device = primary_arms_device
    instruction = PrimaryArmsInstruction.LOWER_MAIN_ARM


class SemiMainArmStep(SimpleStep):
    """
    Give main arm a semi.
    """

    device = primary_arms_device
    instruction = PrimaryArmsInstruction.SEMI_MAIN_ARM


class TriggerExperienceStep(SimpleStep):
    """
    Trigger experience.
    """

    device = primary_arms_device
    instruction = PrimaryArmsInstruction.TRIGGER_EXPERIENCE


class SetSuckersStep(BaseStep):
    name = 'SetSuckersStep'

    def __init__(self, state):
        """
        Set all vacuum relays to the given state. Each relay gets one bit in
        the given byte.
        """

        self.state = state
        super().__init__()

    def run(self):
        send_instruction(
            device=primary_arms_device,
            instruction=PrimaryArmsInstruction.SET_SUCKERS,
            args=[self.state],
        )


class LowerGoldArmStep(BaseStep):

    name = 'LowerGoldArmStep'

    def __init__(self, side):
        """
        Lower the gold arm on given side of robot.
        """

        self.side = side

        super().__init__()

    def run(self):
        send_instruction(
            device=primary_arms_device,
            instruction=PrimaryArmsInstruction.LOWER_GOLD_ARM,
            args=struct.pack('b', self.side.value),
        )


class RaiseGoldArmStep(BaseStep):

    name = 'RaiseGoldArmStep'

    def __init__(self, side):
        """
        Raise the gold arm on given side of robot.
        """

        self.side = side

        super().__init__()

    def run(self):
        send_instruction(
            device=primary_arms_device,
            instruction=PrimaryArmsInstruction.RAISE_GOLD_ARM,
            args=struct.pack('b', self.side.value),
        )
