from .mecanum import Mecanum
from .primary_arms import primary_arms_device
from .primary_sorter import primary_sorter_device
from .primary_accelerator import primary_accelerator_device
from .range_finder import RangeFinder


mecanum = Mecanum()


range_finders = [RangeFinder(pin=i, addr=0x50 + i) for i in range(6)]

i2c_devices = [primary_arms_device, primary_sorter_device,
               primary_accelerator_device]

__all__ = ('mecanum', 'primary_arms_device', 'primary_sorter_device',
           'primary_accelerator_device', 'range_finders')
