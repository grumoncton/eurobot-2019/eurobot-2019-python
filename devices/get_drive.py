from lcd import lcd
from lib import dotenv


ROBOT_NAME = str(dotenv.get_value('ROBOT_NAME'))


def get_drive():
    lcd.display_string('Searching for odrive', line=1)
    lcd.display_string('Open E-Stop', line=2)

    if ROBOT_NAME == 'primary':
        from devices.primary_devices import mecanum
        return mecanum

    if ROBOT_NAME == 'secondary':
        from devices.secondary_devices import wheels
        return wheels

    raise RuntimeError('Could not find drive system')
