class I2CDevice:
    addr = None
    name = None

    def __init__(self):
        self.has_info = False
        self.firmware = None

    def is_connected(self):
        raise NotImplementedError()

    def __repr__(self):
        if self.name:
            return self.name

        if self.has_info:
            return 'device({name})'.format(name=self.name)
        return 'device({addr})'.format(addr=self.addr)

    def __str__(self):
        if self.name:
            return self.name

        if self.has_info:
            return '{name} at 0x{addr:02x} running {firmware}'.format(
                name=self.name,
                addr=self.addr,
                firmware=self.firmware,
            )
        return 'Device(0x{addr:02x})'.format(addr=self.addr)
