from .encoders import encoders
from .wheels import Wheels
from .secondary_mechanisms import secondary_mechanisms_device


wheels = Wheels()


range_finders = []

i2c_devices = [encoders, secondary_mechanisms_device]

__all__ = ('i2c_devices', 'wheels', 'encoders', 'secondary_mechanisms_device',
           'range_finders')
