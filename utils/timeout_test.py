import time
from unittest import TestCase
from unittest.mock import patch

from .timeout import Timeout, TimeoutException


class TimeoutTestCase(TestCase):
    # TODO
    @staticmethod
    @patch('utils.timeout.signal')
    def test_doesnt_raise(mocked_signal):
        with Timeout(seconds=1e-3):
            pass

        # Shouln't raise after block close
        time.sleep(10e-3)

    @patch('utils.timeout.signal')
    def test_raises(self, mocked_signal):
        timeout = Timeout(seconds=1e-3)

        with timeout:
            pass

        mocked_signal.signal.assert_called_with(
            mocked_signal.SIGALRM,
            timeout.handle_timeout,
        )
