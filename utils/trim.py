import textwrap


def trim(string):
    return textwrap.dedent(string).replace('\n', '')
