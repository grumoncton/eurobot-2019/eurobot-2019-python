import uuid
from unittest import TestCase
from unittest.mock import MagicMock

from .retry import Retry


class RetryTestCase(TestCase):
    def test_tries_given_number_of_times(self):

        num_retries = 3
        return_value = uuid.uuid4()

        f = MagicMock()

        # Raise multiple times then succeed
        f.side_effect = [Exception()] * (num_retries - 1) + [return_value]

        res = Retry(num_retries).__call__(f)()

        # Ensure retries
        self.assertEqual(len(f.mock_calls), num_retries)

        # Ensure correct value returned
        self.assertEqual(res, return_value)

    def test_passes_args_and_kwargs(self):
        args = ('foo', 'bar')
        kwargs = {
            'A': 1,
            'B': 2,
        }

        def f(*_args, **_kwargs):
            self.assertEqual(args, _args)
            self.assertEqual(kwargs, _kwargs)

        Retry(1).__call__(f)(*args, **kwargs)

    def test_raises_last_error_after_max_retries(self):
        f = MagicMock()

        side_effect = [ValueError(), TypeError(), KeyError()]
        f.side_effect = side_effect

        with self.assertRaises(KeyError):
            Retry(len(side_effect)).__call__(f)()
