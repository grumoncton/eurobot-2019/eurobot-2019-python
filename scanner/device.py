import subprocess
import serial

from comms.comms import send_command


class Device:
    def __init__(self, port):
        self.port = port
        self.ser = serial.Serial(port)
        self.ser.baudrate = 115200
        self.ser.timeout = None
        self.has_info = False
        self.name = None
        self.firmware = None

    def get_info(self):
        response = send_command(self, 'get_info')
        name, firmware = response.split('.')
        firmware = int(firmware)
        self.name = name
        self.firmware = firmware
        self.has_info = True

        return (name, firmware)

    def is_in_bootloader(self):
        if subprocess.run(
                ['stm32flash', self.port],
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
        ).returncode == 0:
            return True

        return False

    def __repr__(self):
        if self.has_info:
            return f'device({self.name})'
        return f'device({self.port})'

    def __str__(self):
        if self.has_info:
            return f'{self.name} on {self.port} running {self.firmware}'
        return f'device({self.port})'
