import re
import glob

from .device import Device


class DeviceNotFoundException(Exception):
    pass


_port_regex = re.compile('^/dev/tty(USB|ACM)[0-9]+')


def is_port(port):
    """ Check if given string is unix-style port """
    return _port_regex.match(port) is not None


def list_ports():
    """ List all unix-style ports matching a possible device """

    return glob.glob('/dev/ttyUSB[0-9]*')


def map_ports(ports):
    """ Instantiate a device for each port """

    results = []

    for port in ports:
        results.append(Device(port=port))

    return results


def get_device_by_name(name):
    """ Attempt to find serial device matching given name """

    devices = map_ports(list_ports())

    for device in devices:
        try:
            device.get_info()
            if device.name == name:
                return device
        except Exception as exception:
            print(exception)
            continue

    raise DeviceNotFoundException
