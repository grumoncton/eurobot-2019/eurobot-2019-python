def _shorted_angle_to(current, target, expected):
    # delta = current - target
    delta = target - current

    # while delta > 360:
    #     delta -= 360

    # while delta < -360:
    #     delta += 360
    print(delta)

    if delta > 180:
        delta = -360 + delta

    if delta < -180:
        delta = 360 + delta
    # if delta < -180:
    #     delta = -90 - delta

    assert delta == expected, dict(
        current=current,
        target=target,
        expected=expected,
        delta=delta,
    )


_shorted_angle_to(current=90, target=45, expected=-45)
_shorted_angle_to(current=-90, target=-45, expected=45)
_shorted_angle_to(current=0, target=45, expected=45)
_shorted_angle_to(current=-135, target=90, expected=-135)
_shorted_angle_to(current=90, target=-135, expected=135)
_shorted_angle_to(current=-120, target=90, expected=-150)
