#!venv/bin/python

import sys
import time
import click

from cli_commands.run_tests import test, lint
from cli_commands.markdown import markdown
from devices.secondary_mechanisms import \
    cli_commands as secondary_mechanisms_commands


@click.group()
def main():
    pass


main.add_command(secondary_mechanisms_commands.scara_move_to)


@main.command()
@click.option('--addr')
@click.option('--instruction')
@click.option('--args', required=False)
def send_instruction(addr, instruction, args):
    from comms import comms, BaseInstruction
    from devices import I2CDevice
    # import scanner.ports as ports
    # from scanner.device import Device

    device = I2CDevice()
    device.addr = int(addr, 0)

    try:
        instruction = int(instruction, 0)
    except ValueError:
        instruction = getattr(BaseInstruction, instruction.upper())
    # device = Device(port) if ports.is_port(port) \
    #     else ports.get_device_by_name(port)
    comms.send_instruction(
        device=device,
        instruction=instruction,
        args=args,
    )


@main.command()
@click.option('--addr')
@click.option('--instruction')
@click.option('--args', required=False)
def read_data(addr, instruction, args):
    from comms import comms, BaseInstruction
    from devices import I2CDevice
    # import scanner.ports as ports
    # from scanner.device import Device

    device = I2CDevice()
    device.addr = int(addr, 0)

    try:
        instruction = int(instruction, 0)
    except ValueError:
        instruction = getattr(BaseInstruction, instruction.upper())
    # device = Device(port) if ports.is_port(port) \
    #     else ports.get_device_by_name(port)
    click.echo(comms.read_data(
        device=device,
        instruction=instruction,
        args=args,
    ))


@main.command()
@click.argument('colour')
def to_opencv_colours(colour):
    import colorsys
    from colormap import hex2rgb

    r, g, b = hex2rgb(colour)
    h, s, v = colorsys.rgb_to_hsv(r=r, g=g, b=b)
    click.echo('h: {} s: {} v: {}'.format(h * 180, s * 255, v))


@main.command()
@click.argument('device')
def get_port(device):
    import scanner.ports as ports
    click.echo(device)
    click.echo(ports.get_device_by_name(sys.argv[2]).port)


@main.command('curses')
def enter_curses():
    import scanner.curses as curses
    curses.init()


@main.command('crc')
@click.argument('message')
def calc_crc(message):
    from comms.crc import crc16
    crc = crc16(message)
    click.echo(crc)
    click.echo(hex(crc))


@main.command('generate-path')
@click.argument('origin')
@click.argument('destination')
# @click.option('-o', '--output-file', help='Generate image and output to file')
# @click.option('--dpi', type=int, default=200,
#               help='DPI (dots per inch) of output image')
def generate_path(origin, destination):
    from lib.coord import Coord
    from pathfinder import pathfinder
    from pathfinder.visualizer import ResizingCanvas
    from pathfinder import table
    import tkinter as tk
    from os import path

    top = tk.Tk()

    frame = tk.Frame(top, bg='black')
    frame.pack(fill='both', expand=True)

    canvas = ResizingCanvas(
        frame,
        height=table.HEIGHT,
        width=table.HEIGHT,
        highlightthickness=0,
        bgimage=path.join(path.dirname(__file__), './pathfinder/vinyl.png'),
    )

    canvas.pack(fill='both', expand=True)

    origin = Coord.from_string(origin)
    destination = Coord.from_string(destination)
    paths, shortest = pathfinder.shortest_path(origin=origin, destination=destination, canvas=canvas)
    pathfinder.visualizer(paths=paths, canvas=canvas, shortest=shortest)
    canvas.create_line(origin.y, origin.x, destination.y, destination.x, fill='blue')
    top.mainloop()

    # if output_file:
    #     fig.savefig(output_file, dpi=dpi)
    #     click.echo('Path saved to {output_file}.'
    #                .format(output_file=output_file))
    #     return

    # fig.show()
    # input('[Press enter to continue]')


@main.command()
def get_encoder_position():
    from devices import encoders

    click.echo(encoders.get_position())


@main.command()
@click.option('--host', default='barebones')
def deploy(host):
    import subprocess

    DIR = '~/code/eurobot2019/python/'

    subprocess.call([
        'ssh',
        host,
        'mkdir',
        '-p',
        DIR,
    ])

    subprocess.call([
        'rsync',
        '-acv',
        '--no-perms',
        '--omit-dir-times',
        '--delete',
        '--stats',
        '--progress',
        '--exclude=venv',
        '--exclude=__pycache__',
        '--exclude=source',
        '--exclude=storage',
        '--exclude=.git',
        '--exclude=.env',
        '--exclude=*.json',
        '--exclude=*.swp',
        '--exclude=tags',
        './',
        '{host}:{DIR}'.format(host=host, DIR=DIR),
    ])


@main.command()
def lidar_visualizer():
    from lidar.lidar import LidarManager

    lidar = LidarManager(port='/dev/ttyUSB0')
    with lidar.init():
        lidar.visualizer()


main.add_command(test)
main.add_command(lint)
main.add_command(markdown)


if __name__ == '__main__':
    from init.setup_logging import setup_logging

    setup_logging()
    main()
