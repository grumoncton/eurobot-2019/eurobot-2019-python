import time

from devices.primary_devices import range_finders
from devices.range_finder import address_range_finders

address_range_finders(range_finders)

print(range_finders[0].read_distance())

for range_finder in range_finders:
    range_finder._tof.open()
    range_finder.start_ranging()

timing = range_finders[0]._tof.get_timing()
if timing < 20000:
    timing = 20000
print("Timing %d ms" % (timing / 1000))

while True:
    distances = [range_finder._tof.get_distance()
                 for range_finder in range_finders]

    print('Distances:', distances)
    # for range_finder in devices.range_finders:
    #     distance = range_finder.tof.get_distance()
    #     if distance > 0:
    #         print("sensor %s - %d mm, %d cm, iteration %d" % (
    #             range_finder,
    #             distance,
    #             (distance / 10),
    #             count,
    #         ))

    #     else:
    #         print("%s - Error" % range_finder)

    time.sleep(timing / 1000000.00)
