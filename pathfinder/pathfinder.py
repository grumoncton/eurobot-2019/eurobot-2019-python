import math
import itertools
from enum import IntEnum
from collections import deque
from threading import Thread
from pathfinder import table
from shapely.geometry import LineString, Point

from lib import Coord
from lib.functional import pairwise, triplets, insert


AREA_TOLERANCE = 12000

colors = ['red', 'blue', 'pink', 'green', 'black']
j = 0


class InvalidPathError(RuntimeError):
    pass


class Side(IntEnum):
    ABOVE = -1
    BELOW = 1


def without(something, things):
    return [thing for thing in things if thing is not something]


def getExtrapoledLine(p1, p2):
    'Creates a line extrapoled in p1->p2 direction'
    EXTRAPOL_RATIO = 10
    if isinstance(p1, Point):
        p1 = (p1.x, p1.y)
    if isinstance(p2, Point):
        p2 = (p2.x, p2.y)

    a = p1
    b = (
        p1[0] + EXTRAPOL_RATIO * (p2[0] - p1[0]),
        p1[1] + EXTRAPOL_RATIO * (p2[1] - p1[1]),
    )
    return LineString([a, b])


def getDoubleExtrapoledLine(p1, p2):
    line = getExtrapoledLine(p1, p2)
    return getExtrapoledLine(line.coords[1], line.coords[0])


class Pathfinder:

    def __init__(self, permenant_obstacles):
        self.permenant_obstacles = permenant_obstacles
        self.path_obstacles = [
            table.Circle(x=500, y=500, r=200),
            table.Circle(x=1000, y=1500, r=500),
        ]
        self.path = None

    @property
    def obstacles(self):
        return self.permenant_obstacles + self.path_obstacles

    #  Get the distance between two points
    #  Basically just calculate the hypotenuse
    @staticmethod
    def dist_two_points(a, b):
        return math.hypot(a.x - b.x, a.y - b.y)

    @staticmethod
    def instide_tangent(a, b):
        """
        https://stackoverflow.com/questions/27970185/find-line-that-is-tangent-to-2-given-circles
        """

        m = a.r / b.r
        d = math.hypot(a.x - b.x, a.y - b.y)
        h1 = m * d / (1 + m)
        h2 = d / (1 + m)
        d = h1 + h2

        point = Coord(
            x=(h2 * a.x + h1 * b.x) / d,
            y=(h2 * a.y + h1 * b.y) / d,
        )

        return point

    @staticmethod
    def outside_tangent(a, b, side, canvas):
        """
        http://jwilson.coe.uga.edu/emt669/Student.Folders/Kertscher.Jeff/Essay.3/Tangents.html
        """

        (small, big) = sorted((a, b), key=lambda x: x.r)

        d = math.hypot(small.x - big.x, small.y - big.y)
        medium = Point(big.x, big.y).buffer(big.r - small.r).boundary
        middle = Point(
            small.x + (big.x - small.x) / 2,
            small.y + (big.y - small.y) / 2,
        ).buffer(d / 2).boundary

        intersection = medium.intersection(middle)[
            0 if side == Side.ABOVE else 1
        ]

        ray = getExtrapoledLine(
            (big.x, big.y),
            (intersection.x, intersection.y),
        )

        big_point = Point(big.x, big.y).buffer(big.r).boundary.intersection(ray)

        angle = math.atan2(
            big_point.y - intersection.y,
            big_point.x - intersection.x,
        )
        small_point = Point(
            small.x + small.r * math.cos(angle),
            small.y + small.r * math.sin(angle),
        )

        # canvas.create_line(small.y, small.x, small_point.y, small_point.x, fill='red', width=3)
        # canvas.create_line(big.y, big.x, big_point.y, big_point.x, fill='red', width=3)
        # print('big point', big_point, 'small point', small_point)

        # line = getDoubleExtrapoledLine(big_point, small_point)
        # canvas.create_line(line.coords[0][1], line.coords[0][0], line.coords[1][1], line.coords[1][0])
        return getDoubleExtrapoledLine(big_point, small_point)

    def find_tangent(self, circle, point, side, canvas):
        """
        Find tangent points in a circle from a point:
        https://stackoverflow.com/questions/49968720/find-tangent-points-in-a-circle-from-a-point
        """

        if not isinstance(point, Coord):
            other_circle, other_side = point
            print(circle, side, other_side)

            if side == other_side:
                line = self.outside_tangent(circle, other_circle, side=side, canvas=canvas)
                return line
            else:
                print('why is this running?', side, other_side)
                point = self.instide_tangent(circle, other_circle)

        m = -table.WIDTH / table.HEIGHT
        if point.y > m * (point.x - circle.x) + circle.y:
            side *= -1

        dist_to_circle_center = math.hypot(
            point.x - circle.x,
            point.y - circle.y,
        )
        theta = math.acos(circle.r / dist_to_circle_center) * side
        alpha = math.atan2(
            point.y - circle.y,
            point.x - circle.x,
        )

        return getExtrapoledLine((point.x, point.y), (
            circle.x + circle.r * math.cos(theta + alpha),
            circle.y + circle.r * math.sin(theta + alpha),
        ))

    def ensure_point_valid(self, point):
        if 0 < point.x < table.HEIGHT and 0 < point.y < table.WIDTH:
            return point

        raise InvalidPathError()


    def with_obstacles_to_points(self, path, canvas):
        global j
        yield path[0]

        for before, curr, after in triplets(path):
            if isinstance(curr, Coord):
                # self.ensure_point_valid(curr)

                yield curr

            obstacle, side = curr
            print()
            print(before, curr, after)
            if isinstance(obstacle, table.Circle):
                t1 = self.find_tangent(
                    circle=obstacle,
                    point=before,
                    side=side,
                    canvas=canvas,
                )
                t2 = self.find_tangent(
                    circle=obstacle,
                    point=after,
                    side=side,
                    canvas=canvas,
                )
                # color = colors[j % len(colors)]
                # j += 1
                # canvas.create_line(t1.coords[0][1], t1.coords[0][0], t1.coords[1][1], t1.coords[1][0], fill=color)
                # canvas.create_line(t2.coords[0][1], t2.coords[0][0], t2.coords[1][1], t2.coords[1][0], fill=color)
                # print(t1, t2)
                intersection = t1.intersection(t2)

                print('intersection', intersection)

                if not isinstance(intersection, Point):
                    # return Coord(0, 0)
                    raise InvalidPathError()

                # print(intersection)
                yield Coord.from_point(intersection)

        yield path[-1]

    def expand_path(self, path, obstacles, canvas):
        try:
            path_as_points = list(self.with_obstacles_to_points(path, canvas))
        except InvalidPathError:
            return []

        for i, segment in enumerate(pairwise(path_as_points)):
            try:
                blocking = next(
                    obstacle for obstacle in obstacles
                    if obstacle.is_blocking_segment(segment)
                )

                print()
                print('Found obstacle', blocking)
                print()
                print('going above')
                print()
                above = self.expand_path(
                    path=insert(path, i + 1, (blocking, Side.ABOVE)),
                    obstacles=without(blocking, obstacles),
                    canvas=canvas,
                )
                print()
                print('going below')
                print()
                return [
                    *above,
                    *self.expand_path(
                        path=insert(path, i + 1, (blocking, Side.BELOW)),
                        obstacles=without(blocking, obstacles),
                        canvas=canvas,
                    ),
                ]

            except StopIteration:
                continue

        try:
            return [[self.ensure_point_valid(point) for point in path_as_points]]
        except InvalidPathError:
            return []

    def shortest_path(self, origin, destination, canvas):
        # paths = [
        #     [origin, (self.path_obstacles[0], Side.ABOVE), (self.path_obstacles[1], Side.ABOVE), destination],
        #     # [origin, (self.path_obstacles[0], Side.BELOW), (self.path_obstacles[1], Side.BELOW), destination],
        # ]
        # paths = [list(self.with_obstacles_to_points(path, canvas)) for path in paths]
        paths = self.expand_path(path=[origin, destination], obstacles=self.obstacles, canvas=canvas)
        shortest = sorted(paths, key=lambda path: sum(
            self.dist_two_points(*segment)
            for segment in pairwise(path)
        ))[0]
        print(paths)
        return paths, shortest

    def load_path(self, origin, destination):
        def set_path(path):
            self.path = path

        thread = Thread(target=self.shortest_path, daemon=True, kwargs=dict(
            origin=origin,
            destination=destination,
            callback=set_path,
        ))
        thread.start()
        return thread

    def visualizer(self, *args, **kwargs):
        from pathfinder.visualizer import visualize
        return visualize(*args, **kwargs, obstacles=self.obstacles)
