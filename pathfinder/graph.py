from collections import defaultdict


class Graph():
    def __init__(self):
        self.nodes = set()
        self.edges = defaultdict(list)
        self.distances = {}

    def add_edge(self, A, B, distance):
        self.edges[A].append(B)
        self.edges[B].append(A)
        self.distances[frozenset((A, B))] = distance
