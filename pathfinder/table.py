import os
import math
import itertools
from funcy import rpartial
from dotenv import load_dotenv, find_dotenv
from shapely.geometry import Point, LineString, box

from lib.coord import Coord
from lib.timeit import Timeit
from .graph import Graph

# TODO: Move this to main or something
load_dotenv(find_dotenv())

WIDTH = 3000
HEIGHT = 2000

# How close it passes by obstacles
CLEARANCE = 10

assert 'ROBOT_RADIUS' in os.environ, \
    '`ROBOT_RADIUS` missing from `.env`'

MIN_DISTANCE_FROM_OBSTACLE = round(float(os.getenv('ROBOT_RADIUS'))) + CLEARANCE

MIN_X = MIN_DISTANCE_FROM_OBSTACLE
MAX_X = WIDTH - MIN_DISTANCE_FROM_OBSTACLE
MIN_Y = MIN_DISTANCE_FROM_OBSTACLE
MAX_Y = HEIGHT - MIN_DISTANCE_FROM_OBSTACLE

# Grid spacing
# SPACING = 50

# GRID_HEIGHT = ((MAX_Y - MIN_Y) // SPACING) + 1
# GRID_WIDTH = ((MAX_X - MIN_X) // SPACING) + 1
# GRID_SIZE = GRID_WIDTH * GRID_HEIGHT

# If the distance between two points is less than this, a link is made
# POINT_LINK_SPACE = math.sqrt(2) * SPACING + 1


class Obstacle:
    def __init__(self):
        self.shapely = None

    def is_blocking_segment(self, segment):
        line = LineString([(p.x, p.y) for p in segment])
        return not self.shapely.intersection(line).is_empty


class Rectangle(Obstacle):

    # pylint: disable=too-many-instance-attributes

    def __init__(self, points):
        x = sorted(map(rpartial(getattr, 'x'), points))
        y = sorted(map(rpartial(getattr, 'y'), points))

        self.left = min(x)
        self.right = max(x)
        self.bottom = min(y)
        self.top = max(y)

        self.left_padded = self.left - MIN_DISTANCE_FROM_OBSTACLE
        self.right_padded = self.right + MIN_DISTANCE_FROM_OBSTACLE
        self.bottom_padded = self.bottom - MIN_DISTANCE_FROM_OBSTACLE
        self.top_padded = self.top + MIN_DISTANCE_FROM_OBSTACLE

        self.shapely = box(self.left, self.bottom, self.right, self.top)

    def is_blocking_point(self, x, y):
        return self.left_padded < x < self.right_padded and \
            self.bottom_padded < y < self.top_padded


class Circle(Obstacle):
    def __init__(self, x, y, r):
        self.x = x
        self.y = y
        self.r = r
        self.shapely = Point(x, y).buffer(r).boundary

    def is_blocking_point(self, x, y):
        return math.hypot(self.x - x, self.y - y) < \
            self.r + MIN_DISTANCE_FROM_OBSTACLE

    def __str__(self):
        return f'Circle({self.x}, {self.y})'

    def __repr__(self):
        return self.__str__()


obstacles = [

    # Roches
    Circle(x=2000, y=900, r=150),
    Circle(x=2000, y=2100, r=150),

    Circle(x=2000, y=1500, r=300),
]


def dist_two_points(a, b):
    return math.hypot(a.x - b.x, a.y - b.y)


def point_is_blocked(*args, **kwargs):
    return any(obstacle.is_blocking_point(*args, **kwargs)
               for obstacle in obstacles)


# def generate_point_map():
#     return {i: Coord(x, y)
#             for i, (x, y) in enumerate(itertools.product(
#                 range(MIN_X, MAX_X + 1, SPACING),
#                 range(MIN_Y, MAX_Y + 1, SPACING)))
#             if not point_is_blocked(x, y)}


# def generate_edges(points):
#     from enum import IntEnum

#     class Neighbours(IntEnum):
#         ABOVE = 0b01
#         LEFT = 0b10
#         ABOVE_LEFT = ABOVE | LEFT

#     edges = []

#     point_ids = points.keys()
#     first_columns_points = frozenset(range(GRID_HEIGHT))
#     first_rows_points = frozenset(range(0, GRID_SIZE, GRID_HEIGHT))

#     for point in range(GRID_SIZE):
#         point_above = point - 1
#         point_left = point - GRID_HEIGHT
#         point_above_left = point_left - 1

#         hypot = int(SPACING * math.sqrt(2))

#         possible_links = {
#             (point, point_above, SPACING): Neighbours.ABOVE,
#             (point, point_left, SPACING): Neighbours.LEFT,
#             (point, point_above_left, hypot): Neighbours.ABOVE_LEFT,
#             (point_left, point_above, hypot): Neighbours.ABOVE_LEFT,
#         }

#         neighbours = \
#             (Neighbours.ABOVE if point not in first_rows_points else 0) | \
#             (Neighbours.LEFT if point not in first_columns_points else 0)

#         for link, condition in possible_links.items():
#             if link[0] in point_ids and link[1] in point_ids \
#                     and neighbours & condition == condition:
#                 edges.append(link)

#     return edges


# def generate_graph(points, edges):
#     graph = Graph()

#     graph.nodes = frozenset(points.keys())

#     for A, B, dist in edges:
#         graph.add_edge(A, B, dist)

#     return graph


def main():
    pass
    # with Timeit('Generating grid...'):
    #     points = generate_point_map()
    # with Timeit('Calculating edges...'):
    #     edges = generate_edges(points)

    # # store_table(points, edges)

    # with Timeit('Generating graph...'):
    #     graph = generate_graph(points, edges)

    # list(graph.nodes)


if __name__ == '__main__':
    main()
