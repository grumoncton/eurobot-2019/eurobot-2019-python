from .pathfinder import Pathfinder
from pathfinder import table

pathfinder = Pathfinder(permenant_obstacles=table.obstacles)

__all__ = ('Pathfinder', 'pathfinder', 'table')
