import itertools
from os import path
import tkinter as tk
from pathfinder import table
from PIL import Image, ImageTk

from lib.functional import pairwise


def draw_line(canvas, coords, *args, **kwargs):
    b, a, d, c = coords.replace(',', '').split(' ')
    canvas.create_line(a, b, c, d, *args, **kwargs)


class ResizingCanvas(tk.Canvas):
    def __init__(self, parent, bgimage=None, **kwargs):
        tk.Canvas.__init__(self, parent, **kwargs)
        self.bind('<Configure>', self.on_resize)
        self.height = self.winfo_reqheight()
        self.width = self.winfo_reqwidth()
        self.parent = parent
        self.x = 0
        self.y = 0

        self.bgimage = None
        self.canvas_img = None

        if bgimage:
            self.bgimage = Image.open(bgimage)
            self.bgimage_tk = ImageTk.PhotoImage(self.bgimage)
            # self.bgimage_copy = self.bgimage.copy()

            # image_tk = ImageTk.PhotoImage(self.bgimage)
            self.canvas_img = \
                self.create_image(0, 0, image=self.bgimage_tk, anchor='nw')

    def on_resize(self, event):
        new_height = int(min(
            (event.height + 2 * self.y),
            (event.width + 2 * self.x) * table.HEIGHT / table.WIDTH
        ))
        new_width = int(new_height * table.WIDTH / table.HEIGHT)

        scale = float(new_height) / self.height

        self.width = new_width
        self.height = new_height

        self.x = max(0, int((self.parent.winfo_width() - new_width) / 2))
        self.y = max(0, int((self.parent.winfo_height() - new_height) / 2))

        self.config(width=self.width, height=self.height)
        self.parent.config(padx=self.x, pady=self.y)

        self.scale('all', 0, 0, scale, scale)

        if self.bgimage:
            self.bgimage_tk = ImageTk.PhotoImage(self.bgimage.resize(
                (new_width, new_height),
                Image.ANTIALIAS,
            ))
            self.itemconfig(self.canvas_img, image=self.bgimage_tk)


def visualize(paths, obstacles, canvas, shortest):
    for obstacle in obstacles:
        if isinstance(obstacle, table.Rectangle):
            canvas.create_rectangle(
                obstacle.bottom,
                obstacle.left,
                obstacle.top,
                obstacle.right,
                fill='black',
            )

        if isinstance(obstacle, table.Circle):
            canvas.create_oval(
                obstacle.y - obstacle.r,
                obstacle.x - obstacle.r,
                obstacle.y + obstacle.r,
                obstacle.x + obstacle.r,
                fill='black',
                stipple='gray50'
            )

    for points in paths:
        for A, B in pairwise(points):
            canvas.create_line(A.y, A.x, B.y, B.x, fill='black', width=3)

    for A, B in pairwise(shortest):
        canvas.create_line(A.y, A.x, B.y, B.x, fill='red', width=3)
    # draw_line(canvas, '1000, 1500 1490.811841048782 1405.280861551422', fill='red')
    # draw_line(canvas, '500 500 696.3765951313512 462.1023366865795', fill='red')

    # canvas.create_line(300, 200, 1319.81319011055, 1086.791206592967)
    # canvas.create_line(2750, 1750, 1683.994459743455, 921.6033241539271)

    # canvas.create_line(1594.719138448578, 509.1881589512177, -8973.495612902996, -1546.459381874472)
    # draw_line(canvas, '-2800 300, 3200 300', fill='red')
    # draw_line(canvas, '8587.435226549151 9152.905778176275, -5847.658245742455 -6823.966548808717', fill='black')
    # draw_line(canvas, '9435.164300223092 10837.06611019985, -6453.540618125528 -8026.504387097004', fill='blue')
    # draw_line(canvas, '4634.615384615386 8923.076923076924, -1134.615384615386 -4923.076923076924', fill='pink')

    # top.mainloop()
