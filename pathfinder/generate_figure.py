from matplotlib import pyplot as plt
from .table import obstacles, Circle, Rectangle


def generate_figure(points, path):
    x = []
    y = []
    colours = []
    thiccnesses = []

    for point in points:
        x.append(point.x)
        y.append(point.y)
        colours.append('b')
        thiccnesses.append(1)

    #  Make path we'll take red
    for point in path:
        x.append(point.x)
        y.append(point.y)
        colours.append('r')
        thiccnesses.append(20)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(x, y, s=thiccnesses, c=colours)
    ax.set_aspect('equal')
    ax.margins(0)
    ax.set_xlim([0, 2000])
    ax.set_ylim([0, 3000])

    for obstacle in obstacles:
        shape = None

        if isinstance(obstacle, Circle):
            shape = plt.Circle(
                (obstacle.x, obstacle.y),
                obstacle.r,
                color='k'
            )

        elif isinstance(obstacle, Rectangle):
            shape = plt.Rectangle(
                xy=(obstacle.left, obstacle.bottom),
                width=obstacle.right - obstacle.left,
                height=obstacle.top - obstacle.bottom,
            )

        ax.add_artist(shape)

    return fig
