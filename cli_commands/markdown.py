import os
import re
import time
import itertools
import subprocess
import multiprocessing
import click


def generate_module_definitions():
    file_pattern = re.compile(r'^[a-z][a-z_]*\.py$')
    dirname = os.path.dirname(__file__)
    project_root = os.path.dirname(dirname)

    for module in ['comms']:
        files = os.listdir(os.path.join(project_root, module))
        files = filter(file_pattern.match, files)
        files = (os.path.splitext(submodule)[0] for submodule in files
                 if file_pattern.match(submodule) and not
                 submodule.endswith('_test.py'))

        args = itertools.chain(
            iter(['pydocmd', 'simple', module]),
            ('{module}.{submodule}+'.format(
                module=module,
                submodule=submodule,
            ) for submodule in files))

        # Iterable to list
        args = list(args)

        with open('{project_root}/{module}/docs.md'.format(
                project_root=project_root,
                module=module,
        ), 'w') as f:
            subprocess.call(args, stdout=f, cwd=project_root)


def run_preprocessor():
    from MarkdownPP import MarkdownPP, modules

    dirname = os.path.dirname(__file__)
    root = os.path.dirname(dirname)

    with open(os.path.join(root, 'comms/README.template.md'), 'r') as mdpp, \
            open(os.path.join(root, 'comms/README.md'), 'w') as md:
        MarkdownPP(input=mdpp, output=md, modules=list(modules))


def compile_markdown():
    generate_module_definitions()
    run_preprocessor()


@click.command()
@click.option('--watch/--no-watch', default=False,
              help='Watch for changes and re-compile markdown.')
def markdown(watch):
    import grip
    from watchdog.observers import Observer

    from cli_commands.markdown_file_change_handler import \
        MarkdownFileChangeHandler

    compile_markdown()

    if watch:

        # Create a new thread in which to run grip
        grip_process = multiprocessing.Process(
            target=grip.serve,
            kwargs={'path': 'comms/README.md'})

        # Instantiate an event handler for file changes to python and markdown
        # files
        handler = MarkdownFileChangeHandler(compile_markdown,
                                            throttle=2,
                                            include=r'\.(py|md)$',
                                            exclude=r'\.\/(\.git|env)')

        # Create a file watcher
        observer = Observer()
        project_root = os.path.dirname(os.path.dirname(__file__))
        observer.schedule(handler, path=project_root,
                          recursive=True)

        # Start both threads
        grip_process.start()
        observer.start()

        # Let threads work
        while True:
            try:
                time.sleep(1)
            except KeyboardInterrupt:
                grip_process.terminate()
                observer.stop()
                return

        grip_process.join()
        observer.join()
