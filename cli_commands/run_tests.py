import click


@click.command()
def lint():
    import glob
    import subprocess

    child = subprocess.Popen(['pylint', '-j', '8'] + glob.glob('./**/*.py'))
    child.communicate()
    return child.returncode


def run_tests():
    import os
    import subprocess
    from dotenv import dotenv_values
    from funcy import merge

    env = dotenv_values(
        dotenv_path=os.path.join(os.path.dirname(__file__), '../.env'),
    )

    # Run tests in subprocess
    # This is the only way to reliably get coverage
    # Otherwise, lines will be missed in coverage report
    subprocess.call(
        ['coverage', 'run', '--source=.', '--omit', 'env/*',
         '-m', 'unittest', 'discover', '-p', '*_test.py'],
        env=merge(os.environ, env),
    )
    subprocess.call(['coverage', 'html'])


@click.command()
@click.option('--watch/--no-watch', default=False,
              help='Watch for changes and re-run tests.')
def test(watch):

    run_tests()

    # Print coverage report if not in watch mode
    if not watch:
        import subprocess
        subprocess.call(['coverage', 'report'])

    # If in watch mode, setup watcher and server html coverage
    else:
        from livereload import Server

        server = Server()
        server.watch('**/*.py', run_tests, delay=2)

        server.serve(root='htmlcov')
