import time

from comms import read_data, BaseInstruction, CommsStatus


class BaseStep:
    device = None
    name = None

    def __init__(self):
        import logging

        assert self.name is not None
        self.logger = logging.getLogger(self.name)

    def run(self):
        raise NotImplementedError()

    def run_with_queue(self, queue):
        try:
            self.run()

        except Exception as e:  # pylint: disable=broad-except
            queue.put(e)

    def wait_until_ready_state(self):
        while True:
            time.sleep(0.1)

            status = read_data(
                device=self.device,
                instruction=BaseInstruction.GET_STATUS,
            )[0]

            if status == CommsStatus.READY:
                return
