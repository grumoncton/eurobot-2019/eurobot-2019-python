from . import BaseStep


class SimpleReadyStateStep(BaseStep):
    """
    Simple step with constant instruction and args that blocks until ready
    state.
    """

    device = None
    instruction = None
    args = None

    def __init__(self):
        self.name = 'SimpleStep: {}'.format(self.instruction)

        super().__init__()

    def run(self):
        from comms import send_instruction

        send_instruction(
            device=self.device,
            instruction=self.instruction,
            args=self.args,
        )

        self.wait_until_ready_state()
