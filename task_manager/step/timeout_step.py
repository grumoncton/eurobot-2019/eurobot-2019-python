from .base_step import BaseStep


class TimeoutStep(BaseStep):
    """
    Run a step after a given timeout.
    """

    name = 'TimeoutStep'

    def __init__(self, seconds, step=None):
        self.step = step
        self.seconds = seconds

        super().__init__()

    def run(self):
        import time

        time.sleep(self.seconds)

        if self.step:
            self.step.run()
