from .base_step import BaseStep


class SerialStep(BaseStep):

    name = 'SerialStep'

    def __init__(self, steps):
        self.steps = steps
        super().__init__()

    def run(self):
        for step in self.steps:
            step.run()
