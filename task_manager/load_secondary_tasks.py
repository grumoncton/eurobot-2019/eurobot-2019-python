import math

from lib import Coord, RobotSide, dotenv
from devices import wheels
from devices.secondary_mechanisms.steps import OpenPlowStep, ClosePlowStep, \
    DisableVacuumStep, EnableVacuumStep, SplitPlowStep, StraightPlowStep
from devices.secondary_mechanisms.scara_steps import ScaraMoveToStep, \
    ScaraStashStep
from devices.secondary_mechanisms import LeadscrewStep, LeadscrewState
from devices.encoders.steps import EncoderSetPositionStep
from vision import PickUpCaosPuckWithVision, StashCaosPuckWithVision, \
    find_blue, find_green
from lidar import SetLidarToleranceStep
from . import TaskManager, Task, TimeoutStep, SerialStep


BASE_PUCK_TURN_RADIUS = 350

BLUE_BASE_PUCK = Coord(x=1050, y=500)
RED_BASE_PUCK = Coord(x=450, y=500)

IS_HOMO = bool(dotenv.get_value('IS_HOMO'))


def load_secondary_tasks():
    if IS_HOMO:
        return TaskManager(tasks=[
            Task(name='Homo', score=0, steps=[
                [
                    SplitPlowStep(),
                    wheels.ChangeConfigStep(
                        speed=10e3,
                        step=wheels.RotateAroundPointStep(
                            heading=math.radians(0),
                            side=RobotSide.LEFT,
                            radius=744.399,
                        ),
                    ),
                ],
                [
                    OpenPlowStep(),
                    wheels.ChangeConfigStep(
                        speed=10e3,
                        step=wheels.MoveToStep(Coord(x=750, y=400)),
                    ),
                ],
            ]),
        ])

    return TaskManager(tasks=[
        Task(name='Red pucks', score=6 * 4, steps=[

            # Collect caos zone
            [
                SplitPlowStep(),
                wheels.RotateAroundPointStep(
                    heading=math.radians(0),
                    side=RobotSide.LEFT,
                    radius=744.399,
                ),
            ],
            [
                OpenPlowStep(),
                wheels.MoveToStep(Coord(x=1040, y=700)),
            ],

            # Run vision on caos pucks
            StashCaosPuckWithVision(
                puck_finder=find_blue,
                side=RobotSide.RIGHT,
            ),
            StashCaosPuckWithVision(
                puck_finder=find_green,
                side=RobotSide.LEFT,
            ),

            # Collect base pucks
            SetLidarToleranceStep(
                tolerance=100,
                step=wheels.MoveToStep(BLUE_BASE_PUCK + Coord(x=0, y=50)),
            ),

            wheels.DriveStraightStep(
                distance=200,
                direction=wheels.Wheels.Direction.BACKWARDS,
            ),

            [
                ClosePlowStep(),
                SetLidarToleranceStep(
                    tolerance=100,
                    step=SerialStep(steps=[
                        wheels.RotateToStep(heading=0),
                        wheels.RotateAroundPointStep(
                            heading=math.radians(-90),
                            side=RobotSide.LEFT,
                            radius=BASE_PUCK_TURN_RADIUS - 30,
                        ),
                    ],)
                ),
            ],

            [
                wheels.RotateToStep(math.radians(180)),
                OpenPlowStep(),
            ],

            wheels.MoveToStep(RED_BASE_PUCK + Coord(x=150, y=0)),
            wheels.DriveStraightStep(
                distance=220,
                direction=wheels.Wheels.Direction.BACKWARDS,
            ),

            [
                ClosePlowStep(),
                wheels.RotateToStep(heading=math.radians(90)),
            ],

            wheels.RotateAroundPointStep(
                heading=math.radians(180),
                side=RobotSide.RIGHT,
                radius=BASE_PUCK_TURN_RADIUS,
            ),

            [
                wheels.RotateToStep(math.radians(-90)),
                OpenPlowStep(),
            ],

            SetLidarToleranceStep(
                tolerance=100,
                step=wheels.DriveStraightStep(distance=500),
            ),

            PickUpCaosPuckWithVision(puck_finder=find_green),

            wheels.DriveStraightStep(
                distance=200,
                direction=wheels.Wheels.Direction.BACKWARDS,
            ),
        ]),

        Task(name='Green puck', score=6, steps=[
            ClosePlowStep(),

            wheels.MoveToStep(Coord(x=800, y=400, a=0)),

            ScaraMoveToStep(Coord(x=120, y=100)),
            LeadscrewStep(state=LeadscrewState.PUCK_PICK_UP_HEIGHT),

            DisableVacuumStep(),

            LeadscrewStep(state=LeadscrewState.CLEAR_STASH_HEIGHT),

            OpenPlowStep(),
            ClosePlowStep(),
            wheels.RotateToStep(0),
            wheels.DriveStraightStep(distance=300),
        ]),

        Task(name='Ramp', score=8 * 2 + 12, steps=[

            # Home y
            [
                ScaraMoveToStep(Coord(x=160, y=0)),
                SetLidarToleranceStep(tolerance=100, step=wheels.MoveToStep(
                    Coord(x=1400, y=150, a=math.radians(-90)),
                )),
            ],

            wheels.ChangeConfigStep(
                speed=5e3,
                step=SetLidarToleranceStep(
                    tolerance=0,
                    step=wheels.DriveStraightStep(
                        distance=200,
                    ),
                ),
            ),

            EncoderSetPositionStep(Coord(x=1400, y=40, a=math.radians(-90))),

            # Back away from wall
            wheels.DriveStraightStep(
                distance=170,
                direction=wheels.Wheels.Direction.BACKWARDS,
            ),

            wheels.MoveToStep(Coord(x=1950, y=210)),

            # Home x
            wheels.ChangeConfigStep(
                speed=5e3,
                step=SetLidarToleranceStep(
                    tolerance=0,
                    step=wheels.DriveStraightStep(
                        distance=200,
                    ),
                ),
            ),

            EncoderSetPositionStep(Coord(x=1960, y=225, a=0)),

            # Back away from wall
            wheels.DriveStraightStep(
                distance=170,
                direction=wheels.Wheels.Direction.BACKWARDS,
            ),

            # Line up for ramp
            wheels.RotateToStep(heading=math.radians(90)),

            [
                wheels.DriveStraightStep(distance=100),
                StraightPlowStep(),
            ],

            LeadscrewStep(state=LeadscrewState.CLEAR_STASH_HEIGHT),
            ScaraStashStep(side=RobotSide.LEFT),
            EnableVacuumStep(),
            LeadscrewStep(state=LeadscrewState.STASH_HEIGHT),
            LeadscrewStep(state=LeadscrewState.CLEAR_STASH_HEIGHT),
            ScaraMoveToStep(Coord(x=0, y=160)),

            DisableVacuumStep(),

            TimeoutStep(seconds=0.5),

            ScaraStashStep(side=RobotSide.RIGHT),

            EnableVacuumStep(),
            LeadscrewStep(state=LeadscrewState.STASH_HEIGHT),
            LeadscrewStep(state=LeadscrewState.CLEAR_STASH_HEIGHT),
            ScaraMoveToStep(Coord(x=0, y=160)),

            [
                SetLidarToleranceStep(
                    tolerance=0,
                    step=wheels.ChangeConfigStep(
                        speed=25e3,
                        step=wheels.DriveStraightStep(distance=2000),
                    ),
                ),
                TimeoutStep(seconds=2, step=SerialStep(steps=[
                    DisableVacuumStep(),
                    ScaraMoveToStep(Coord(x=160, y=0)),
                ])),
            ],
        ]),


        # Stay in closed loop
        Task(name='Wait until over', score=0, steps=[
            TimeoutStep(step=None, seconds=100e3),
        ]),
    ])
