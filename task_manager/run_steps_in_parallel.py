import time
import queue
from threading import Thread


def run_steps_in_parallel(steps):
    exception_queue = queue.Queue()

    threads = tuple(Thread(
        target=step.run_with_queue,
        kwargs={'queue': exception_queue},
        daemon=True,
    ) for step in steps)

    for thread in threads:
        thread.start()

    while True:
        if not any(thread.is_alive() for thread in threads):
            break

        try:
            exception = exception_queue.get_nowait()

            raise exception

        except queue.Empty:
            time.sleep(0.1)

    for thread in threads:
        thread.join()
