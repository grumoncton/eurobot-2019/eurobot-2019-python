from lcd import lcd


class TaskManager:
    def __init__(self, tasks):
        import logging

        self.score = 0
        self.tasks = tasks
        self.logger = logging.getLogger(__name__)

    def get_new_task(self):
        try:
            task = self.tasks.pop(0)
            return task
        except IndexError:
            raise StopIteration()

    def loop(self):
        try:
            while True:

                task = self.get_new_task()
                task.run()

                self.score += task.score

                self.logger.info(
                    'Done task "%s". New score: %d',
                    task.name,
                    self.score,
                )

                lcd.clear()
                lcd.display_string(str(self.score), line=1)

        except StopIteration:
            pass
