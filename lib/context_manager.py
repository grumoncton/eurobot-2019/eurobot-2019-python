class ContextManager:
    def __init__(self, on_exit):
        self.on_exit = on_exit

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs):
        self.on_exit()
