import math
from math import sin, cos


class Coord:
    FORMAT_STRING = '<fff'

    def __init__(self, x, y, a=None):
        self.x = x
        self.y = y
        self.a = a

    def angle_to(self, other):
        delta = other - self
        return math.atan2(delta.y, delta.x)

    def hypot(self):
        return math.hypot(self.x, self.y)

    def __str__(self):
        if self.a:
            return 'Coord: ({x:.2f}, {y:.2f}, {a:.02f})'.format(
                x=self.x,
                y=self.y,
                a=math.degrees(self.a),
            )
        return 'Coord: ({x:.2f}, {y:.2f})'.format(x=self.x, y=self.y)

    def __repr__(self):
        return self.__str__()

    def __add__(self, other):
        if self.a:
            return Coord(
                x=self.x + other.x * sin(self.a) + other.y * cos(self.a),
                y=self.y + other.y * sin(self.a) - other.x * cos(self.a),
                a=self.a,
            )

        return Coord(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        # TODO: a
        return Coord(self.x - other.x, self.y - other.y)

    def __mul__(self, factor):
        # TODO: a
        return Coord(self.x * factor, self.y * factor)

    def __rmul__(self, factor):
        # TODO: a
        return self.__mul__(factor)

    @classmethod
    def from_polar(cls, r, a):
        a = math.radians(a)

        return cls(
            x=r * math.cos(a),
            y=r * math.sin(a),
            a=a,
        )

    @classmethod
    def from_string(cls, string):
        x, y = str(string).split(',')
        return cls(x=float(x), y=float(y))

    @classmethod
    def from_bytes(cls, data):
        import struct

        size = struct.calcsize(cls.FORMAT_STRING)
        x, y, a = struct.unpack(
            cls.FORMAT_STRING,
            bytes(data[:size]),
        )

        return cls(x=x, y=y, a=a)

    @classmethod
    def from_point(cls, point):
        """
        From shapely Point
        """

        return cls(
            x=point.x,
            y=point.y,
        )

    def to_bytes(self):
        import struct

        return struct.pack(
            self.FORMAT_STRING,
            float(self.x),
            float(self.y),
            float(self.a),
        )

    def to_prime(self):
        """
        Convert a normal (yellow) coordinate to it's prime (purple) counterpart.
        """

        return Coord(
            y=self.y,
            x=-self.x,
            a=math.pi - self.a if self.a else None,
        )

    def from_prime(self):
        """
        Convert a prime (purple) coordinate to it's normal (yellow) counterpart.
        """

        return Coord(
            y=self.y,
            x=-self.x,
            a=math.pi - self.a if self.a else None,
        )

    def merge_with(self, other):
        """
        Merge a new coordinate taking the angle of theirs defaulting to the
        angle of ours.
        """

        return Coord(
            x=other.x,
            y=other.y,
            a=self.a or other.a,
        )
