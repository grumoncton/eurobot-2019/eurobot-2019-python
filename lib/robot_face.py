from enum import IntEnum


class RobotFace(IntEnum):
    """
    Representation of the front-to-back side of the robot.
    """

    FRONT = 1
    BACK = -1
