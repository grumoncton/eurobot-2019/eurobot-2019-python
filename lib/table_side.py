from enum import Enum


class TableSide(Enum):
    """
    Starting side of table.

    Everything is based off yellow side, so using `RobotSide.RIGHT` is only the
    right side of the robot on the yellow side of the table.
    """

    YELLOW = 'YELLOW'
    PURPLE = 'PURPLE'
