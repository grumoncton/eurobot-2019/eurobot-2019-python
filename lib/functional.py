import itertools
from funcy import autocurry


fgetattr = autocurry(lambda key, o: getattr(o, key))


def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)


def triplets(iterable):
    for i in range(1, len(iterable) - 1):
        yield (iterable[i - 1], iterable[i], iterable[i + 1])


def insert(iterable, i, item):
    return iterable[:i] + [item] + iterable[i:]
