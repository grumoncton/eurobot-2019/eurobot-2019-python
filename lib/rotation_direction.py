from enum import IntEnum


class RotationDirection(IntEnum):
    CW = -1
    CCW = 1
