import time
import logging


class Timeit:
    logger = logging.getLogger(__name__)

    def __init__(self, message, level=logging.DEBUG):
        self.message = message
        self.level = level
        self.start_time = None

    def __enter__(self):
        if self.logger.getEffectiveLevel() >= self.level:
            self.start_time = time.time()

    def __exit__(self, *args, **kwargs):
        if self.logger.getEffectiveLevel() >= self.level:
            self.logger.log(
                self.level,
                '%(message)s took %(seconds)f seconds.',
                {
                    'message': self.message,
                    'seconds': round(time.time() - self.start_time, 4),
                },
            )
