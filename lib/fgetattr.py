def fgetattr(path, d):
    if not path:
        return d

    return fgetattr(path[1:], getattr(d, path[0]))
