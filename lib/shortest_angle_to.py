import math


def shortest_angle_to(current, target):
    """
    Get the shortest angle we would have to rotate to point towards target
    angle.
    """

    delta = target - current

    if delta > math.pi:
        delta = -2 * math.pi + delta

    if delta < -math.pi:
        delta = 2 * math.pi + delta

    return delta
