import time
import threading


time_up_event = threading.Event()


def handle_time_up():
    time.sleep(100)
    time_up_event.set()


def start_timing():
    threading.Thread(
        daemon=True,
        target=handle_time_up,
    ).start()
